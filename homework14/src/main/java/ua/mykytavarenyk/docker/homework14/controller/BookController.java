package ua.mykytavarenyk.docker.homework14.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.mykytavarenyk.docker.homework14.model.Book;
import ua.mykytavarenyk.docker.homework14.service.BookService;

import java.util.Optional;

@RestController
@RequestMapping("/books")
public class BookController {
    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/{page}/{size}/{sortBy}/{direction}")
    public Iterable<Book> getAll(@PathVariable(name = "page") Integer page,
                                 @PathVariable(name = "size") int size,
                                 @PathVariable(name = "sortBy") String sortBy,
                                 @PathVariable(name = "direction") String direction) {
        return bookService.findAll(page, size, sortBy, direction);
    }

    @GetMapping("/{id}")
    public Optional<Book> getBookById(@PathVariable long id) {
        return bookService.findById(id);
    }

    @PostMapping
    public void createBook(Book book) {
        bookService.createBook(book);
    }

    @DeleteMapping("/{id}")
    public void deleteBook(@PathVariable long id) {
        bookService.deleteById(id);
    }

    @PatchMapping("/{id}")
    public void updateBook(@PathVariable long id, @RequestBody Book book) {
        bookService.updateBook(id, book);
    }
}

