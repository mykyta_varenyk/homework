package ua.mykytavarenyk.docker.homework14.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import ua.mykytavarenyk.docker.homework14.model.Book;

import java.util.Optional;

public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
    Page<Book> findAll(Pageable pageable);

    Optional<Book> findById(Long id);

    Book save(Book book);

    @Override
    void deleteById(Long id);
}

