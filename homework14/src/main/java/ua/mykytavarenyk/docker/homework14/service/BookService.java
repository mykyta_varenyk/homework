package ua.mykytavarenyk.docker.homework14.service;

import ua.mykytavarenyk.docker.homework14.model.Book;

import java.util.Optional;

public interface BookService {
    Optional<Book> findById(Long id);

    Iterable<Book> findAll(int page, int size, String sort, String direction);

    Book createBook(Book book);

    void deleteById(long id);

    void updateBook(long id, Book changedBook);
}
