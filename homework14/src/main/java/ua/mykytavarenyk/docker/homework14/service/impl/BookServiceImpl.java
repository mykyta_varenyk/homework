package ua.mykytavarenyk.docker.homework14.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.mykytavarenyk.docker.homework14.model.Book;
import ua.mykytavarenyk.docker.homework14.repository.BookRepository;
import ua.mykytavarenyk.docker.homework14.service.BookService;

import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Optional<Book> findById(Long id) {
        return bookRepository.findById(id);
    }

    @Override
    public Iterable<Book> findAll(int page, int size, String sortBy, String direction) {
        Sort sort = Sort.by(sortBy);

        if ("desc".equals(direction)) {
            sort.descending();
        }
        return bookRepository.findAll(PageRequest.of(page - 1, 4, sort));
    }

    @Override
    public Book createBook(Book book) {
        return bookRepository.save(book);
    }

    @Override
    @Transactional
    public void deleteById(long id) {
        bookRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void updateBook(long id, Book changedBook) {
        bookRepository.findById(id)
                .map(book -> {
                    book.setAuthor(changedBook.getAuthor());
                    book.setName(changedBook.getName());
                    return bookRepository.save(book);
                })
                .orElseGet((() -> {
                    changedBook.setId(id);
                    return bookRepository.save(changedBook);
                }));
    }
}

