package ua.mykytavarenyk.homework15;

import java.util.UUID;

public class ErrorMessage {
    private UUID wrongId;

    private String errorMessage;

    public UUID getWrongId() {
        return wrongId;
    }

    public void setWrongId(UUID wrongId) {
        this.wrongId = wrongId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ErrorMessage(UUID wrongId, String errorMessage) {
        this.wrongId = wrongId;
        this.errorMessage = errorMessage;
    }

    public ErrorMessage() {

    }

    @Override
    public String toString() {
        return "ErrorMessage{" +
                "wrongId=" + wrongId +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
