package ua.mykytavarenyk.homework15;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Service
public class ErrorReceiver {
    private static final Logger LOGGER = LogManager.getLogger(ErrorReceiver.class);

    @JmsListener(destination = "invalid-messages")
    public void receiveErrorMessage(ErrorMessage errorMessage) {
        LOGGER.debug(errorMessage);
    }
}
