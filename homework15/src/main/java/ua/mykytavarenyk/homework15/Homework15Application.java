package ua.mykytavarenyk.homework15;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.security.SecureRandom;
import java.util.Random;

@SpringBootApplication
public class Homework15Application implements ApplicationRunner {

    @Autowired
    private MessageSender sender;

    private static final Random random = new SecureRandom();

    public static void main(String[] args) {
        SpringApplication.run(Homework15Application.class, args);
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        for (int i = 0; i < 10; i++) {
            sender.sendQueue(new Request(random.nextInt(10), random.nextInt(10)));
        }
    }
}
