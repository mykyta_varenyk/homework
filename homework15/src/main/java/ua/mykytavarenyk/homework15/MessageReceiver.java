package ua.mykytavarenyk.homework15;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static ua.mykytavarenyk.homework15.config.ActiveMQConfiguration.QUEUE;

@Service
public class MessageReceiver {
    private static final Logger LOGGER = LogManager.getLogger(MessageReceiver.class);

    private List<Request> listOfRequests = new LinkedList<>();

    @Autowired
    private JmsTemplate template;

    @JmsListener(destination = QUEUE)
    public void sum(Request request) {
        LOGGER.debug("Received requests from {}", request);

        listOfRequests.add(request);

        if (listOfRequests.size() >= 10) {
            for (int i = 0; i < listOfRequests.size(); i++) {

                Request currentRequest = listOfRequests.get(i);
                int result = currentRequest.getFirstOperand() + currentRequest.getSecondOperand();

                LOGGER.debug("Returned {} ", result);

                if (i == 8) {
                    template.convertAndSend("message-reply", new Response(result, UUID.randomUUID()));
                    continue;
                }
                template.convertAndSend("message-reply", new Response(result, currentRequest.getCorrelationId()));
            }
        }
    }
}
