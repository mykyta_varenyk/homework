package ua.mykytavarenyk.homework15;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import static ua.mykytavarenyk.homework15.config.ActiveMQConfiguration.QUEUE;

@Service
public class MessageSender {
    private static final Logger LOGGER = LogManager.getLogger(MessageSender.class);

    private Map<UUID, Request> requests = new HashMap<>();

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendQueue(Request request) {
        LOGGER.debug("Sending {}", request);

        requests.put(request.getCorrelationId(), request);

        jmsTemplate.convertAndSend(QUEUE, request);
    }

    @JmsListener(destination = "message-reply")
    public void receiveReply(Response response) {
        if (!requests.containsKey(Objects.requireNonNull(response).getCorrelationId())) {
            jmsTemplate.convertAndSend("invalid-messages",
                    new ErrorMessage(response.getCorrelationId(), "No such correlationId"));
        } else {
            Request currentRequest = requests.get(response.getCorrelationId());

            int expectedResult = currentRequest.getFirstOperand() + currentRequest.getSecondOperand();

            if (expectedResult != response.getOperationResult()) LOGGER.error("wrong result");
        }
    }
}
