package ua.mykytavarenyk.homework15;

import java.io.Serializable;
import java.util.UUID;

public class Request implements Serializable {
    private int firstOperand;

    private int secondOperand;

    private UUID correlationId = UUID.randomUUID();

    public Request(int firstOperand, int secondOperand) {
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
    }

    public Request() {

    }

    public int getFirstOperand() {
        return firstOperand;
    }

    public void setFirstOperand(int firstOperand) {
        this.firstOperand = firstOperand;
    }

    public int getSecondOperand() {
        return secondOperand;
    }

    public void setSecondOperand(int secondOperand) {
        this.secondOperand = secondOperand;
    }

    public UUID getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(UUID correlationId) {
        this.correlationId = correlationId;
    }

    @Override
    public String toString() {
        return "Message{" +
                "firstOperand=" + firstOperand +
                ", secondOperand=" + secondOperand +
                '}';
    }
}
