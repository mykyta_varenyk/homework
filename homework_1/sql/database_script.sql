create table book
(
    id            int auto_increment
        primary key,
    date_of_issue date null,
    count         int not null,
    date          varchar(45) null
);

create table language
(
    id           int auto_increment
        primary key,
    name         varchar(45) not null,
    languagecode varchar(2)  not null
);

create table author
(
    id          int         not null,
    name        varchar(45) not null,
    first_name  varchar(45) not null,
    last_name   varchar(45) not null,
    lan_id      int         not null,
    middle_name varchar(45) null,
    primary key (id, lan_id),
    constraint ln_id
        foreign key (lan_id) references language (id)
);

create
index ln_id_idx
    on author (lan_id);

create table book_has_author
(
    book_id   int not null,
    author_id int not null,
    primary key (book_id, author_id),
    constraint author_id
        foreign key (author_id) references author (id),
    constraint book_id
        foreign key (book_id) references book (id)
            on update cascade on delete cascade
);

create
index author_id_idx
    on book_has_author (author_id);

create table book_translation
(
    book        int         not null,
    language_id int         not null,
    name        varchar(45) not null,
    description varchar(45) null,
    publisher   varchar(45) not null,
    primary key (book, language_id),
    constraint book
        foreign key (book) references book (id)
            on update cascade on delete cascade,
    constraint language_id
        foreign key (language_id) references language (id)
);

create
index language_id_idx
    on book_translation (language_id);

create table role
(
    id   int not null
        primary key,
    name enum ('user', 'librarian', 'admin') not null
);

create table user
(
    id          int auto_increment
        primary key,
    login       varchar(16)         not null,
    email       varchar(30) null,
    password    varchar(32)         not null,
    create_time timestamp default CURRENT_TIMESTAMP null,
    role_id     int                 not null,
    first_name  varchar(45) null,
    last_name   varchar(45) null,
    is_blocked  int       default 0 not null,
    constraint fk_account_role1
        foreign key (role_id) references role (id)
);

create table `order`
(
    id                 int auto_increment,
    approved_time      timestamp null,
    account_id         int not null,
    given_by_librarian int default 0 null,
    is_approved        int default 0 null,
    in_reading_hall    int default 0 null,
    expired_at         date null,
    is_returned        int null,
    days_count         int default 0 null,
    hours_count        int default 0 null,
    primary key (id, account_id),
    constraint fk_abonement_account1
        foreign key (account_id) references user (id)
);

create
index fk_abonement_account1_idx
    on `order` (account_id);

create table order_has_book
(
    order_id int not null,
    book_id  int not null,
    primary key (order_id, book_id),
    constraint fk_order_has_book_book1
        foreign key (book_id) references book (id),
    constraint fk_order_has_book_order1
        foreign key (order_id) references `order` (id)
);

create
index fk_order_has_book_book1_idx
    on order_has_book (book_id);

create
index fk_order_has_book_order1_idx
    on order_has_book (order_id);

create
index fk_account_role1_idx
    on user (role_id);
