package ua.dnipro.ntudp.varenyk_mykyta.library.db;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import com.mysql.cj.jdbc.MysqlDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * DB manager. Works with MySQL.
 *
 * @author Mykyta Varenyk
 */

@Component
public class DBManager {
    private static DBManager instance;

    private static Logger LOGGER = LogManager.getLogger(DBManager.class);

    public static final String SQL_SELECT_ALL_LANGUAGES = "SELECT name FROM language";

    public static final String SQL_SELECT_COUNT_OF_LANGUAGES = "SELECT COUNT(id) FROM language;";

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    /**
     * Returns a DB connection from the Pool Connections. Before using this
     * method you must configure the Date Source and the Connections Pool in your
     * webapp/META-INF/context.xml file.
     *
     * @return A DB connection.
     */
    public Connection getConnection() {
        Connection con = null;
        try {
            Context initialContext = new InitialContext();
            Context envContext = (Context) initialContext.lookup("java:/comp/env");
            DataSource dataSource = (DataSource) envContext.lookup("jdbc/library");
            con = dataSource.getConnection();
        } catch (NamingException | SQLException e) {
            LOGGER.error("");
            e.printStackTrace();
        }
        return con;
    }

    @Bean
    public static DataSource getDataSource() throws NamingException {
        Context envContext = null;
        try {
            Context initialContext = new InitialContext();
            envContext = (Context) initialContext.lookup("java:/comp/env");
        } catch (NamingException e) {
            LOGGER.debug("NamingException in DBManager#getDataSource() -> {}", e);
        }
        return (DataSource) envContext.lookup("jdbc/library");
    }


    public static DataSource getDataSourceForTests() {
        MysqlDataSource mysqlDataSource = null;
        try {
            mysqlDataSource = new MysqlConnectionPoolDataSource();
            mysqlDataSource.setURL("jdbc:mysql://localhost:3306/library");
            mysqlDataSource.setUser("root");
            mysqlDataSource.setPassword("15092023Mykyta");
            mysqlDataSource.setServerTimezone("UTC");
            mysqlDataSource.setAllowMultiQueries(true);
        } catch (SQLException throwables) {
            LOGGER.debug("SQLException in DBManager#getDataSourceForTests() -> {}", throwables);
        }
        return mysqlDataSource;
    }

    /**
     * Returns a DB connection. This method is just for a example how to use the
     * DriverManager to obtain a DB connection. It does not use a pool
     * connections and not used in this project. It is preferable to use
     * {@link #getConnection()} method instead.
     *
     * @return A DB connection.
     */
    public Connection getConnectionWithDriverManager() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/library?\n" +
                    "        autoReconnect=true&" +
                    "        useSSL=false&\n" +
                    "        useLegacyDatetimeCode=false&\n" +
                    "        serverTimezone=UTC" +
                    "&username=root&password=15092023Mykyta");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }

    /**
     * Commits and close the given connection.
     *
     * @param con connection, on which a commit and close operations must be executed.
     */
    public static void commitAndClose(Connection con) {
        try {
            con.commit();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Rollbacks and closes the given connection.
     *
     * @param con connection, on which a rollback and close operations must be executed.
     */
    public static void rollbackAndClose(Connection con, Savepoint point) {
        try {
            con.rollback(point);
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Returns all languages.
     *
     * @return A list of all languages.
     */
    public static List<String> getLanguages() {
        List<String> languages = new ArrayList<>();

        try (Connection con = DBManager.getInstance().getConnection();
             Statement statement = con.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_LANGUAGES)) {
            while (resultSet.next()) {
                languages.add(resultSet.getString("name"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return languages;
    }

    public static int getNumberOfLanguages() {
        int numberOfLanguages = 0;

        try (Connection con = DBManager.getDataSource().getConnection();
             Statement statement = con.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_COUNT_OF_LANGUAGES)) {
            if (resultSet.next()) {
                numberOfLanguages = resultSet.getInt(1);
            }
        } catch (SQLException | NamingException throwables) {
            throwables.printStackTrace();
        }
        return numberOfLanguages;
    }
}
