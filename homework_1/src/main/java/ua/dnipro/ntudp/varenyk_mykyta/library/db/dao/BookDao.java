package ua.dnipro.ntudp.varenyk_mykyta.library.db.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Author;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Data access object for book related entities.
 */

@Repository
public class BookDao {
    public static Logger LOGGER = LogManager.getLogger(BookDao.class);

    public static final String SQL_SELECT_AUTHOR_BY_ID = "SELECT a.name FROM author a, language l WHERE l.languagecode=? AND a.lan_id=l.id AND a.id=?;";

    public static final String SQL_SELECT_ALL_AUTHORS = "SELECT a.id, a.name " +
            "FROM author a, language l " +
            "WHERE l.languagecode=? AND l.id=a.lan_id;";

    public static final String SQL_SELECT_BOOKS_WITH_PAGINATION = "SELECT b.id, b.count, b.date_of_issue, bt.name, bt.publisher, bt.description" +
            " FROM book b, book_translation bt, language l WHERE l.languagecode=? AND bt.language_id=l.id AND b.id=bt.book LIMIT ?,?;";

    public static final String SQL_SELECT_ALL_BOOK_NAMES = "SELECT b.id, bt.name" +
            " FROM book b, book_translation bt, language l" +
            " WHERE l.languagecode=? AND bt.language_id=l.id AND bt.book=b.id";

    public static final String SQL_SELECT_ALL_PUBLISHERS = "SELECT bt.publisher" +
            " FROM book_translation bt, language l" +
            " WHERE l.languagecode=? AND bt.language_id=l.id;";

    public static final String SQL_SELECT_ALL_DATES_OF_ISSUE = "SELECT date_of_issue FROM book;";

    public static final String SQL_SELECT_ALL_BOOKS = "SELECT b.id, b.count, b.date_of_issue, bt.name, bt.publisher, bt.description " +
            " FROM book b, book_translation bt, language l WHERE l.languagecode=? AND bt.language_id=l.id AND b.id=bt.book";


    public static final String SQL_SELECT_AUTHORS_FOR_BOOK = "SELECT a.name FROM author a, language l  WHERE l.languagecode=? AND a.lan_id=l.id AND a.id  IN (SELECT author_id FROM book_has_author WHERE book_id =?);";

    public static final String SQL_SELECT_BOOK_BEANS_BY_NAME = "SELECT b.id, b.date_of_issue, b.count, bt.name, bt.publisher" +
            " FROM book b, book_translation bt, language l" +
            " WHERE l.languagecode=? AND bt.language_id=l.id AND bt.name=? AND bt.book=b.id ORDER BY b.id;";

    public static final String SQL_SELECT_BOOK_BEANS_BY_AUTHOR = "SELECT b.id, b.date_of_issue, b.count, bt.name, bt.publisher, bt.description" +
            " FROM book b, book_has_author bha, author a, language l, book_translation bt" +
            " WHERE l.languagecode=? AND a.lan_id=l.id" +
            " AND a.name=? AND bha.author_id=a.id AND b.id=bha.book_id AND bt.book=b.id AND bt.language_id= l.id ORDER BY b.id;";

    public static final String SQL_DELETE_BOOK_BY_ID = "DELETE FROM book WHERE id=?; DELETE FROM book_translation WHERE book=?;";

    public static final String SQL_INSERT_NEW_BOOK = "INSERT INTO book (date_of_issue, count) VALUES (?,?) ";

    public static final String SQL_INSERT_DATA_INTO_BOOK_TRANSLATION_TABLE = "INSERT INTO book_translation(book, language_id, name, description, publisher) VALUES (?,?,?,?,?);" +
            "INSERT INTO book_translation(book, language_id, name, description, publisher) VALUES (?,?,?,?,?);";

    public static final String SQL_CONNECT_AUTHOR_TO_BOOK = "INSERT INTO book_has_author VALUES (?,?)";

    public static final String SQL_SELECT_BOOK_BY_ID = "SELECT b.id, b.date_of_issue, b.count, bt.description, bt.publisher, bt.name" +
            " FROM book b, book_translation bt, language l" +
            " WHERE l.languagecode=? AND bt.language_id=l.id AND b.id=bt.book AND b.id=?;";

    public static final String SQL_SELECT_BOOK_BY_ID_ANOTHER_TRANSLATION = "SELECT bt.publisher, bt.description, bt.name" +
            " FROM book_translation bt, language l" +
            " WHERE l.languagecode !=? AND bt.language_id=l.id AND bt.book=?";

    public static final String SQL_UPDATE_BOOK = "UPDATE book b SET b.date_of_issue=?, b.count=?  WHERE b.id=?;";

    public static final String SQL_UPDATE_BOOK_TRANSLATION = "UPDATE book_translation  SET name=?, description=?, publisher=?" +
            " WHERE language_id =? AND book=?;";
    public static final String SQL_UPDATE_AUTHOR_FOR_BOOK = "UPDATE book_has_author " +
            "SET author_id=? " +
            "WHERE book_Id=?";

    public static final String SQL_SELECT_BOOK_BEANS_BY_NAME_AND_AUTHORS = "SELECT b.id, b.date_of_issue, b.count, bt.name, bt.publisher" +
            " FROM book b, book_has_author bha, author a, language l, book_translation bt" +
            " WHERE l.languagecode=? AND a.lan_id=l.id" +
            " AND a.name=? AND bha.author_id=a.id AND b.id=bha.book_id AND bt.book=b.id AND bt.language_id= l.id AND bt.name =? ORDER BY b.id;";

    public static final String SQL_SELECT_BOOKS_COUNT = "SELECT COUNT(id) FROM book;";

    private DataSource dataSource;

    @Autowired
    public BookDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public BookDao() {
    }

    /**
     * Returns all books.
     *
     * @param languageCode a language code of internationalized fields
     * @return List of book entities.
     */
    public List<Book> getAllBooks(String languageCode) {
        List<Book> books = new ArrayList<>();
        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(SQL_SELECT_ALL_BOOKS)) {
            statement.setString(1, languageCode);
            try (ResultSet resSet = statement.executeQuery()) {
                BookMapper mapper = new BookMapper();
                while (resSet.next()) {
                    Book book = mapper.mapRow(resSet);

                    book.setAuthor(BookMapper.mapAuthors(book.getId(), con, languageCode));

                    books.add(book);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return books;
    }

    /**
     * Returns the amount of distinct book types.
     * <p>
     * Used in pair with {@code BookDao#getBooks(int, int, String))} to implement pagination
     *
     * @return Number of book types in library(integer value).
     * @see BookDao#getBooks(int, int, String)
     */
    public int getNumberOfBooks() {
        int numberOfBooks = 0;
        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_BOOKS_COUNT)) {
            if (resultSet.next()) {
                numberOfBooks = resultSet.getInt(1);
            }
        } catch (SQLException throwables) {

            throwables.printStackTrace();
        }
        return numberOfBooks;
    }

    /**
     * Returns books with specified limit and offset.
     * <p>
     * Used in pair with {@code BookDao#getNumberOfBooks()} to implement pagination
     *
     * <pre> Code example
     *      int currentPage = 1;
     *
     *      int recordsPerPage = 2;
     *
     *      if (request.getParameter("page") != null){
     *           currentPage = Integer.parseInt(req.getParameter("page"));
     *       }
     *
     *       User user = (User) session.getAttribute("user");
     *
     *       int id = user.getId();
     *
     *       OrderDao orderDao = new OrderDao();
     *
     *       int rows = orderDao.getOrdersAmountForUser(id);
     *
     *       int numOfPages = rows/recordsPerPage;
     *
     *       if (numOfPages % recordsPerPage > 0){
     *           numOfPages++;
     *       }
     *
     *       List<UserOrderBean> userOrderBeans = orderDao.getUserOrderBeansByUserId(id,languageCode,currentPage,recordsPerPage);
     * </pre>
     *
     * @param currentPage    limit
     * @param recordsPerPage offset
     * @param languageCode   a language code of internationalized fields
     * @return List of book entities with specified limit and offset.
     * @see BookDao#getNumberOfBooks()
     */
    public List<Book> getBooks(int currentPage, int recordsPerPage, String languageCode) {
        List<Book> books = new LinkedList<>();

        int start = currentPage * recordsPerPage - recordsPerPage;

        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(SQL_SELECT_BOOKS_WITH_PAGINATION)) {
            statement.setString(1, languageCode);
            statement.setInt(2, start);
            statement.setInt(3, recordsPerPage);

            try (ResultSet resSet = statement.executeQuery()) {
                BookMapper mapper = new BookMapper();
                while (resSet.next()) {
                    Book book = mapper.mapRow(resSet);

                    book.setAuthor(BookMapper.mapAuthors(book.getId(), con, languageCode));

                    books.add(book);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return books;
    }

    /**
     * Returns all author names.
     *
     * @param languageCode a language code of internationalized fields
     * @return List of author entities.
     */
    public List<Author> getAllAuthors(String languageCode) {
        List<Author> authors = new ArrayList<>();
        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(SQL_SELECT_ALL_AUTHORS)) {
            statement.setString(1, languageCode);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Author author = new Author();
                    author.setId(resultSet.getInt("id"));
                    author.setName(resultSet.getString("name"));
                    authors.add(author);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return authors;
    }

    /**
     * Returns all author names.
     *
     * @param languageCode a language code of internationalized fields
     * @return List of author entities.
     */
    public List<Book> getAllBookNames(String languageCode) {
        List<Book> books = new ArrayList<>();
        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(SQL_SELECT_ALL_BOOK_NAMES)) {
            statement.setString(1, languageCode);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Book book = new Book();
                    book.setId(resultSet.getInt(Fields.ENTITY_ID));
                    book.setName(resultSet.getString(Fields.ENTITY_NAME));
                    books.add(book);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return books;
    }

    /**
     * Returns all publisher names.
     *
     * @param languageCode a language code of internationalized fields
     * @return List of publisher names.
     */
    public List<String> getAllPublishers(String languageCode) {
        List<String> publishers = new ArrayList<>();

        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(SQL_SELECT_ALL_PUBLISHERS)) {
            statement.setString(1, languageCode);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    publishers.add(resultSet.getString(Fields.BOOK_PUBLISHER));
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return publishers;
    }

    /**
     * Returns all dates of issue.
     *
     * @return List of dates of issue .
     */
    public List<LocalDateTime> getAllDatesOfIssue() {
        List<LocalDateTime> datesOfIssues = new LinkedList<>();

        try (Connection con = dataSource.getConnection();
             Statement statement = con.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_DATES_OF_ISSUE)) {
            while (resultSet.next()) {
                datesOfIssues.add(resultSet.getObject(1, LocalDateTime.class));
            }

            datesOfIssues = new ArrayList<>(datesOfIssues);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return datesOfIssues;
    }

    /**
     * Returns an author name for a book.
     *
     * @param id           book id
     * @param languageCode language of the author name
     * @return author name.
     */
    public String getAuthorById(int id, String languageCode) {
        String name = "";
        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(SQL_SELECT_AUTHOR_BY_ID)) {
            statement.setString(1, languageCode);
            statement.setInt(2, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    name = resultSet.getString("name");
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return name;
    }

    /**
     * Returns a book by a specified id.
     *
     * @param id           book id
     * @param languageCode language of internationalized fields
     * @return A book by specified id.
     */
    public Book getBookById(int id, String languageCode) {
        Book book = new Book();

        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(SQL_SELECT_BOOK_BY_ID)) {
            statement.setString(1, languageCode);
            statement.setInt(2, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                BookMapper mapper = new BookMapper();
                if (resultSet.next()) {
                    book = mapper.mapRow(resultSet);
                    book.setAuthor(BookMapper.mapAuthors(book.getId(), con, languageCode));
                }

                try (PreparedStatement pStatement = con.prepareStatement(SQL_SELECT_BOOK_BY_ID_ANOTHER_TRANSLATION)) {
                    pStatement.setString(1, languageCode);
                    pStatement.setInt(2, id);
                    try (ResultSet resSet = pStatement.executeQuery()) {
                        if (resSet.next()) {
                            book.setPublisherUa(resSet.getString(Fields.BOOK_PUBLISHER));
                            book.setNameUa(resSet.getString(Fields.ENTITY_NAME));
                            book.setDescriptionUa(resSet.getString(Fields.BOOK_DESCRIPTION));
                        }
                    }
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return book;
    }

    /**
     * Inserts a book into DB.
     *
     * @param book     a book to be inserted into a DB
     * @param authorId an author id, by which book and author/s will be connected
     * @return {@code true} if operation was successful,
     * {@code false} otherwise.
     */
    public boolean createBook(Book book, int authorId) {
        boolean result = false;
        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(SQL_INSERT_NEW_BOOK, Statement.RETURN_GENERATED_KEYS)) {
            statement.setDate(1, Date.valueOf(book.getDate()));
            statement.setInt(2, book.getCount());
            result = statement.executeUpdate() == 1;
            ResultSet resultSet = statement.getGeneratedKeys();
            int generatedId = 0;
            if (resultSet.next()) {
                generatedId = resultSet.getInt(1);
            }

            if (result) {
                try (PreparedStatement preparedStatement = con.prepareStatement(SQL_INSERT_DATA_INTO_BOOK_TRANSLATION_TABLE)) {
                    preparedStatement.setInt(1, generatedId);
                    preparedStatement.setInt(2, 1);
                    preparedStatement.setString(3, book.getName());
                    preparedStatement.setString(4, book.getDescription());
                    preparedStatement.setString(5, book.getPublisher());
                    preparedStatement.setInt(6, generatedId);
                    preparedStatement.setInt(7, 2);
                    preparedStatement.setString(8, book.getNameUa());
                    preparedStatement.setString(9, book.getDescriptionUa());
                    preparedStatement.setString(10, book.getPublisherUa());
                    result = preparedStatement.executeUpdate() == 1;
                    if (result == true) {
                        try (PreparedStatement pStatement = con.prepareStatement(SQL_CONNECT_AUTHOR_TO_BOOK)) {
                            pStatement.setInt(1, generatedId);
                            pStatement.setInt(2, authorId);
                            result = pStatement.executeUpdate() == 1;
                        }
                    }
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    /**
     * Update a book.
     *
     * @param book a book to be updated
     * @return {@code true} if operation was successful,
     * {@code false} otherwise.
     */
    public boolean updateBook(Book book, int authorId) {
        boolean result = false;
        Connection con = null;
        Savepoint point = null;
        PreparedStatement statement = null;
        try {
            con = dataSource.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);

            point = con.setSavepoint();

            statement = con.prepareStatement(SQL_UPDATE_BOOK);
            System.out.println(book.getDate());
            statement.setDate(1, Date.valueOf(book.getDate()));
            statement.setInt(2, book.getCount());
            statement.setInt(3, book.getId());
            result = statement.executeUpdate() == 1;

            if (result) {
                statement = con.prepareStatement(SQL_UPDATE_BOOK_TRANSLATION);
                statement.setString(1, book.getName());
                statement.setString(2, book.getDescription());
                statement.setString(3, book.getPublisher());
                statement.setInt(4, 1);
                statement.setInt(5, book.getId());
                statement.addBatch();
                statement.setString(1, book.getNameUa());
                statement.setString(2, book.getDescriptionUa());
                statement.setString(3, book.getPublisherUa());
                statement.setInt(4, 2);
                statement.setInt(5, book.getId());
                statement.addBatch();
                int[] results = statement.executeBatch();
                for (int res : results) {
                    if (res != 1) {
                        con.rollback(point);
                        result = false;
                    }
                }
                if (result) {
                    statement = con.prepareStatement(SQL_UPDATE_AUTHOR_FOR_BOOK);
                    statement.setInt(1, authorId);
                    statement.setInt(2, book.getId());
                    result = statement.executeUpdate() == 1;
                    if (!result) {
                        con.rollback(point);
                    } else {
                        con.commit();
                    }
                }
            } else {
                con.rollback(point);
            }
        } catch (SQLException throwables) {
            LOGGER.debug("SQLException in BookDao#updateBook(Book,int) -> {}", throwables);
            DBManager.rollbackAndClose(con, point);

            throwables.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                    if (statement != null) {
                        statement.close();
                    }
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return result;
    }

    /**
     * Delete a book by id.
     *
     * @param id a specified book id
     * @return {@code true} if operation was successful,
     * {@code false} otherwise.
     */
    public boolean deleteBookById(int id) {
        boolean result = false;
        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(SQL_DELETE_BOOK_BY_ID)) {
            statement.setInt(1, id);
            statement.setInt(2, id);
            result = statement.executeUpdate() == 1;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }

    /**
     * Returns books that have specified author and name.
     *
     * @param languageCode a language code of internationalized fields
     * @param author       an author name
     * @param name         a name of a book
     * @return List of matching books.
     */
    public List<Book> findBookBeansByNameAndAuthor(String languageCode, String author, String name) {
        List<Book> bookBeans = new ArrayList<>();

        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(SQL_SELECT_BOOK_BEANS_BY_NAME_AND_AUTHORS)) {
            statement.setString(1, languageCode);
            statement.setString(2, author);
            statement.setString(3, name);
            try (ResultSet resultSet = statement.executeQuery()) {
                BookMapper mapper = new BookMapper();

                while (resultSet.next()) {
                    Book bean = mapper.mapRow(resultSet);

                    bean.setAuthor(BookMapper.mapAuthors(bean.getId(), con, languageCode));

                    bookBeans.add(bean);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return bookBeans;
    }

    /**
     * Returns books that have specified name.
     *
     * @param languageCode a language code of internationalized fields
     * @param name         a name of a book
     * @return List of matching books.
     */
    public List<Book> findBookBeansByName(String languageCode, String name) {

        List<Book> bookBeans = new ArrayList<>();

        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(SQL_SELECT_BOOK_BEANS_BY_NAME)) {
            statement.setString(1, languageCode);

            statement.setString(2, name);

            ResultSet resultSet = statement.executeQuery();

            BookMapper mapper = new BookMapper();

            while (resultSet.next()) {
                Book bean = mapper.mapRow(resultSet);

                bean.setAuthor(BookMapper.mapAuthors(bean.getId(), con, languageCode));

                bookBeans.add(bean);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return bookBeans;
    }

    /**
     * Returns books that have specified author/s.
     *
     * @param languageCode a language code of internationalized fields
     * @param authors      a name/s of author/s
     * @return List of matching books.
     */
    public List<Book> findBookBeansByAuthors(String languageCode, String... authors) {
        List<Book> bookBeans = new LinkedList<>();

        try (Connection con = dataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(SQL_SELECT_BOOK_BEANS_BY_AUTHOR)) {
            statement.setString(1, languageCode);
            statement.setString(2, authors[0]);

            try (ResultSet resSet = statement.executeQuery()) {
                BookMapper mapper = new BookMapper();

                while (resSet.next()) {
                    Book bean = mapper.mapRow(resSet);

                    bean.setAuthor(BookMapper.mapAuthors(bean.getId(), con, languageCode));

                    bookBeans.add(bean);
                }

                bookBeans = new ArrayList<>(bookBeans);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return bookBeans;
    }

    /**
     * Extracts a book entity from the result set row.
     */
    private static class BookMapper implements EntityMapper<Book> {

        public static List<String> mapAuthors(int id, Connection con, String languageCode) {
            List<String> authors = new ArrayList<>();
            try (PreparedStatement statement = con.prepareStatement(SQL_SELECT_AUTHORS_FOR_BOOK)) {
                statement.setString(1, languageCode);
                statement.setInt(2, id);
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        authors.add(resultSet.getString(Fields.ENTITY_NAME));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return authors;
        }

        @Override
        public Book mapRow(ResultSet rs) {
            Book book = new Book();
            try {
                book.setId(rs.getInt(Fields.ENTITY_ID));
                book.setName(rs.getString(Fields.ENTITY_NAME));
                book.setPublisher(rs.getString(Fields.BOOK_PUBLISHER));
                book.setCount(rs.getInt(Fields.BOOK_COUNT));
                if (rs.getDate(Fields.BOOK_DATE_OF_ISSUE) != null) {
                    book.setDateOfIssue(rs.getDate(Fields.BOOK_DATE_OF_ISSUE).toLocalDate());
                }
                book.setDate(String.valueOf(rs.getDate(Fields.BOOK_DATE_OF_ISSUE)));
                book.setDescription(rs.getString(Fields.BOOK_DESCRIPTION));

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            return book;
        }
    }
}
