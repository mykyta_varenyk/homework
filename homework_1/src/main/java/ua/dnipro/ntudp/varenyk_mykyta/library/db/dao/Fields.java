package ua.dnipro.ntudp.varenyk_mykyta.library.db.dao;


/**
 * Holder for fields names of DB tables and beans.
 *
 * @author Mykyta Varenyk
 */
public class Fields {
    public static final String ENTITY_ID = "id";
    public static final String ENTITY_NAME = "name";

    public static final String USER_LOGIN = "login";
    public static final String USER_PASSWORD = "password";
    public static final String USER_FIRST_NAME = "first_name";
    public static final String USER_LAST_NAME = "last_name";
    public static final String USER_CREATE_TIME = "create_time";
    public static final String USER_ROLE_ID = "role_id";
    public static final String USER_EMAIL = "email";
    public static final String USER_STATUS = "is_blocked";
    public static final String USER_ROLE = "role";
    public static final String USER_HAS_ORDERS = "has_orders";

    public static final String ORDER_APPROVED_TIME = "approved_time";
    public static final String ORDER_USER_ID = "account_id";
    public static final String ORDER_GIVEN_BY_LIBRARIAN_ID = "given_by_librarian";
    public static final String ORDER_IS_APPROVED = "is_approved";
    public static final String ORDER_IN_READING_HALL = "in_reading_hall";
    public static final String ORDER_EXPIRED_AT = "expired_at";
    public static final String ORDER_DAYS_COUNT = "days_count";
    public static final String ORDER_HOURS_COUNT = "hours_count";
    public static final String ORDER_IS_RETURNED = "is_returned";

    public static final String ORDER_HAS_BOOK_ORDER_ID = "order_id";
    public static final String ORDER_HAS_BOOK_BOOK_ID = "book_id";

    public static final String BOOK_DATE_OF_ISSUE = "date_of_issue";
    public static final String BOOK_COUNT = "count";
    public static final String BOOK_DESCRIPTION = "description";
    public static final String BOOK_PUBLISHER = "publisher";
    public static final String BOOK_AUTHOR = "author";
    public static final String BOOK_DESCRIPTION_UA = "descriptionUa";
    public static final String BOOK_PUBLISHER_UA = "publisherUa";
    public static final String BOOK_NAME_UA = "nameUa";
}
