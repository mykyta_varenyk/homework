package ua.dnipro.ntudp.varenyk_mykyta.library.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpSession;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {
    private static final Logger LOGGER = LogManager.getLogger(Util.class);

    public static boolean validatePassword(String password) {
        Pattern pattern = Pattern.compile("^(?=.*[A-Z])(?=.*\\d).*$");
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean validateEmail(String email) {
        Pattern pattern = Pattern.compile("\\w{3,}@\\w+.([a-z]{2,4})");
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String getLanguageCodeFromSession(HttpSession session) {
        String languageCode;

        if (session != null) {
            languageCode = String.valueOf(session.getAttribute("language"));
        } else {
            languageCode = "en";
        }

        LOGGER.debug("language code -> {}", languageCode);
        return languageCode;
    }

}

