package ua.dnipro.ntudp.varenyk_mykyta.library.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.DBManager;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.Fields;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Author;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Book;
import ua.dnipro.ntudp.varenyk_mykyta.library.util.Util;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.BookService;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.ServiceUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@Controller
public class BookController {
    private static Logger LOGGER = LogManager.getLogger(BookController.class);

    @Autowired
    private BookService bookService;

    @Autowired
    private OrderDao orderDao;

    @GetMapping(value = "*/search")
    @ResponseBody
    public ModelAndView findBooks(HttpServletRequest request, ModelAndView modelAndView) {
        String searchByAuthor = request.getParameter("search-by-author");

        String searchByName = request.getParameter("search-by-name");

        String authorId = request.getParameter("author-id");

        String nameId = request.getParameter("name-id");

        HttpSession session = request.getSession();

        String languageCode = Util.getLanguageCodeFromSession(session);

        List<Book> books = bookService.findBooks(languageCode, searchByAuthor, searchByName, authorId, nameId);

        if (books == null) {
            String errorMessage = "No arguments have been selected";
            LOGGER.error("ERROR: {}", errorMessage);
            modelAndView = new ModelAndView(Path.PAGE_ERROR_PAGE);
            modelAndView.addObject("errorMessage", errorMessage);
            return modelAndView;
        }

        List<UserOrderBean> userOrderBeans = orderDao.getAllUserOrderBeans(languageCode);

        LOGGER.debug("get orders -> {}", userOrderBeans);

        modelAndView = new ModelAndView(Path.PAGE_SEARCH);

        modelAndView.addObject("bookBeans", books);

        modelAndView.addObject("userOrderBeans", userOrderBeans);

        return modelAndView;
    }

    @GetMapping(value = "*/sort")
    @ResponseBody
    public ModelAndView sortBooks(HttpServletRequest req) {
        ModelAndView modelAndView;

        HttpSession session = req.getSession();

        String sortBy = req.getParameter("sort_by");

        LOGGER.debug("selected sorting parameter -> {}", sortBy);

        List<Book> booksList = (List<Book>) session.getAttribute("booksList");

        LOGGER.debug("presorted books -> {}", booksList);

        booksList = bookService.sortBooks(booksList, sortBy);

        LOGGER.debug("sorted catalog -> {}", booksList);

        req.getSession().setAttribute("booksList", booksList);

        modelAndView = new ModelAndView(Path.PAGE_INDEX_REDIRECT);

        modelAndView.addObject("booksList", booksList);

        return modelAndView;
    }

    @PostMapping(value = "*/change-language")
    @ResponseBody
    public ModelAndView changeLanguage(HttpServletRequest req) {
        bookService.changeLanguage(req);

        return new ModelAndView(req.getParameter("pageToReturn"));
    }

    @GetMapping(value = "*/admin/books")
    @ResponseBody
    public ModelAndView getAdminBooksPage(HttpServletRequest req, ModelAndView modelAndView) {
        HttpSession session = req.getSession();

        modelAndView = new ModelAndView(Path.PAGE_ADMIN_BOOKS_PAGE);

        String languageCode = Util.getLanguageCodeFromSession(session);

        if (req.getParameter("dateOfIssueInTheFuture") != null) {
            LOGGER.debug(req.getParameter("dateOfIssueInTheFuture"));

            modelAndView.addObject("dateOfIssueInTheFuture", req.getParameter("dateOfIssueInTheFuture"));
        }

        int currentPage = 1;

        int recordsPerPage = 2;

        if (req.getParameter("page") != null) {
            currentPage = Integer.parseInt(req.getParameter("page"));
        }

        LOGGER.debug("currentPage -> {}", currentPage);

        List<Book> books = bookService.getBooksWithPagination(currentPage, recordsPerPage, languageCode);

        modelAndView.addObject("books", books);

        List<Author> authors = bookService.getAllAuthors(languageCode);

        LOGGER.debug("authors -> {}", authors);

        List<String> languages = DBManager.getLanguages();

        LOGGER.debug("languages -> {}", languages);

        modelAndView.addObject("currentPage", currentPage);

        modelAndView.addObject("recordsPerPage", recordsPerPage);

        modelAndView.addObject("numOfPages", ServiceUtil.getNumberOfPages(bookService.getAmountOfBooks(), recordsPerPage));

        modelAndView.addObject("authors", authors);

        modelAndView.addObject("languages", languages);

        return modelAndView;
    }

    @PostMapping(value = "*/admin/create-book")
    @ResponseBody
    public ModelAndView createBook(HttpServletRequest req) {
        HttpSession session = req.getSession();

        String errorMessage, languageCode = Util.getLanguageCodeFromSession(session);

        int authorId = Integer.parseInt(req.getParameter("authorId"));

        LOGGER.debug("author id -> {}", authorId);

        if (validateDate(req.getParameter(Fields.BOOK_DATE_OF_ISSUE))) {
            errorMessage = ResourceBundle.getBundle("text", Locale.forLanguageTag(languageCode))
                    .getString("label.dateOfIssueInTheFuture");

            session.setAttribute("dateOfIssueInTheFuture", errorMessage);

            LOGGER.error("errorMessage -> {}", errorMessage);

            return new ModelAndView(Path.PAGE_ADMIN_BOOKS_PAGE_REDIRECT);
        }

        bookService.createBook(req, authorId, languageCode);

        return new ModelAndView(Path.PAGE_ADMIN_BOOKS_PAGE_REDIRECT);
    }

    private boolean validateDate(String dateString) {
        LocalDate date1 = LocalDate.parse(dateString);

        LOGGER.debug("user given date -> {}", date1);

        return !date1.isBefore(LocalDate.now());
    }
}
