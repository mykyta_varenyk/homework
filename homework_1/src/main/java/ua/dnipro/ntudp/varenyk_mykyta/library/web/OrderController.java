package ua.dnipro.ntudp.varenyk_mykyta.library.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;
import ua.dnipro.ntudp.varenyk_mykyta.library.util.Util;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class OrderController {
    private static Logger LOGGER = LogManager.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @GetMapping(value = "*/user/orders")
    @ResponseBody
    public ModelAndView userOrders(HttpServletRequest req) {
        LOGGER.debug("OrdersController#userOrders started");

        HttpSession session = req.getSession();

        String languageCode = Util.getLanguageCodeFromSession(session);

        int currentPage = 1;

        int recordsPerPage = 2;

        if (req.getParameter("page") != null) {
            currentPage = Integer.parseInt(req.getParameter("page"));
        }

        LOGGER.debug("currentPage -> {}", currentPage);

        User user = (User) session.getAttribute("user");

        int id = user.getId();

        LOGGER.debug("user id -> {}", id);

        List<UserOrderBean> userOrderBeans = orderService.getUserOrderBeansWithPagination(currentPage, recordsPerPage, languageCode, id);

        LOGGER.debug("userOrderBeans with limit:{} and offset:{} -> {}", currentPage, recordsPerPage, userOrderBeans);

        ModelAndView modelAndView = new ModelAndView("user/orders");

        modelAndView.addObject("userOrderBeans", userOrderBeans);

        modelAndView.addObject("currentPage", currentPage);

        modelAndView.addObject("recordsPerPage", recordsPerPage);

        modelAndView.addObject("numOfPages", orderService.countOrdersByUserId(id, recordsPerPage));

        return modelAndView;
    }


}
