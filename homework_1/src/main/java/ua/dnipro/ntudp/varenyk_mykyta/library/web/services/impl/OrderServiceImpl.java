package ua.dnipro.ntudp.varenyk_mykyta.library.web.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.bean.UserOrderBean;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.OrderDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.OrderService;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.ServiceUtil;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    private static Logger LOGGER = LogManager.getLogger(OrderServiceImpl.class);

    private OrderDao orderDao;

    @Autowired
    public OrderServiceImpl(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Override
    public int countOrdersByUserId(int id, int recordsPerPage) {
        return ServiceUtil.getNumberOfPages(orderDao.getOrdersAmountForUser(id), recordsPerPage);
    }

    @Override
    public List<UserOrderBean> getUserOrderBeansWithPagination(int currentPage, int recordsPerPage, String languageCode, int id) {
        int rows = orderDao.getOrdersAmountForUser(id);

        LOGGER.debug("rows -> {}", rows);

        int numOfPages = (int) Math.ceil(rows * 1.0 / recordsPerPage);

        LOGGER.debug("numOfPages -> {}", numOfPages);

        List<UserOrderBean> userOrderBeans = orderDao.getUserOrderBeansByUserId(id, languageCode, currentPage, recordsPerPage);

        LOGGER.debug("userOrderBeans with limit:{} and offset:{} -> {}", currentPage, recordsPerPage, userOrderBeans);

        return orderDao.getUserOrderBeansByUserId(id, languageCode, currentPage, recordsPerPage);
    }
}
