package ua.dnipro.ntudp.varenyk_mykyta.library.web.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;
import ua.dnipro.ntudp.varenyk_mykyta.library.Path;
import ua.dnipro.ntudp.varenyk_mykyta.library.db.dao.UserDao;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.Role;
import ua.dnipro.ntudp.varenyk_mykyta.library.model.User;
import ua.dnipro.ntudp.varenyk_mykyta.library.web.services.UserService;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@Service
public class UserServiceImpl implements UserService {
    private static Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);

    private UserDao userDao;

    @Autowired
    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public ModelAndView validateLoginInformation(String login, String password, String languageCode, HttpSession session) {
        LOGGER.debug("UserService#validateLoginInformation(String,String,String,HttpSession) started");

        ModelAndView modelAndView = new ModelAndView(Path.PAGE_LOGIN);

        String errorMessage;

        User user = userDao.findUserByLogin(login);

        if (user.getLogin() == null) {
            errorMessage = ResourceBundle.getBundle("text", Locale.forLanguageTag(languageCode))
                    .getString("label.userNotFound");

            LOGGER.error("errorMessage -> {}", errorMessage);

            session.setAttribute("userNotFound", errorMessage);

            return modelAndView;
        } else if (!user.getPassword().equals(password)) {
            errorMessage = ResourceBundle.getBundle("text", Locale.forLanguageTag(languageCode))
                    .getString("label.incorrectPasswordError");

            LOGGER.error("errorMessage -> {}", errorMessage);

            session.setAttribute("incorrectPasswordError", errorMessage);

            return modelAndView;
        } else if (user.isBlocked()) {
            errorMessage = "You are blocked!";

            LOGGER.error("errorMessage -> {}", errorMessage);

            modelAndView = new ModelAndView(Path.PAGE_ERROR_PAGE);

            modelAndView.addObject("errorMessage", errorMessage);

            return modelAndView;
        } else {
            Role userRole = Role.getRole(user);

            if (userRole == Role.ADMIN) {
                modelAndView = new ModelAndView(Path.PAGE_ADMIN_HOME_PAGE_REDIRECT);
            }

            if (userRole == Role.USER) {
                modelAndView = new ModelAndView(Path.PAGE_USER_HOME_PAGE_REDIRECT);
            }

            if (userRole == Role.LIBRARIAN) {
                modelAndView = new ModelAndView(Path.PAGE_LIBRARIAN_HOME_PAGE_REDIRECT);
            }

            LOGGER.debug("userRole -> {}", userRole);

            session.setAttribute("user", user);

            session.setAttribute("userRole", userRole);

            LOGGER.debug("UserService#validateLoginInformation(String,String,String,HttpSession) finished");

            return modelAndView;
        }
    }

    @Override
    public List<User> getUsersWithPagination(int currentPage, int recordsPerPage) {
        return userDao.getUsers(currentPage, recordsPerPage);
    }

    @Override
    public int countUsers() {
        return userDao.getNumberOfUsers();
    }
}
