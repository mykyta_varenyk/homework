package ua.mykyta_varenyk.hibernate.assembler;

import ua.mykyta_varenyk.hibernate.dto.OrderDto;
import ua.mykyta_varenyk.hibernate.entity.Order;

public interface OrderAssembler {
    Order assemble(OrderDto orderDto);

    OrderDto assemble(Order entity);
}
