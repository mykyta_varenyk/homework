package ua.mykyta_varenyk.hibernate.assembler;

import ua.mykyta_varenyk.hibernate.dto.UserDto;
import ua.mykyta_varenyk.hibernate.entity.User;

public interface UserAssembler {
    User assemble(UserDto userDto);

    UserDto assemble(User entity);
}
