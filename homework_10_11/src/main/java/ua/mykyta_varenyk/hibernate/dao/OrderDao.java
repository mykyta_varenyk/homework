package ua.mykyta_varenyk.hibernate.dao;

import org.springframework.transaction.annotation.Transactional;
import ua.mykyta_varenyk.hibernate.entity.Order;

import java.util.List;
import java.util.UUID;

public interface OrderDao {
    @Transactional
    void save(Order order);

    Order get(UUID orderId);

    Order get(String bookName);

    void delete(UUID orderId);

    List<Order> getAll();
}
