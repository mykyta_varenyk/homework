package ua.mykyta_varenyk.hibernate.entity;

import javax.persistence.*;

@Entity
@NamedQueries(@NamedQuery(name = "roleByRoleName",
        query = "from Role r where r.roleName=:roleName"))
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "role")
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
