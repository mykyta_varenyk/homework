package ua.mykyta_varenyk.hibernate.service;

import org.springframework.transaction.annotation.Transactional;
import ua.mykyta_varenyk.hibernate.dto.OrderDto;
import ua.mykyta_varenyk.hibernate.entity.Order;

import java.util.List;
import java.util.UUID;

public interface OrderService {
    @Transactional
    OrderDto create(OrderDto dto);

    @Transactional(readOnly = true)
    OrderDto get(String bookName);

    @Transactional
    OrderDto update(OrderDto dto);

    @Transactional
    void delete(UUID id);

    OrderDto get(UUID id);

    List<Order> getAll();
}
