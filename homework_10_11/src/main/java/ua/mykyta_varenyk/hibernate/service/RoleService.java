package ua.mykyta_varenyk.hibernate.service;

import org.springframework.transaction.annotation.Transactional;
import ua.mykyta_varenyk.hibernate.entity.Role;

import java.util.List;

public interface RoleService {
    @Transactional
    Role create(Role role);

    @Transactional(readOnly = true)
    Role get(String roleName);

    @Transactional
    Role update(Role role);

    @Transactional
    void delete(Integer id);

    @Transactional(readOnly = true)
    List<Role> getAll();
}
