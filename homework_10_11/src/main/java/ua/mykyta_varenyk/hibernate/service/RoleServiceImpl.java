package ua.mykyta_varenyk.hibernate.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.mykyta_varenyk.hibernate.dao.RoleDao;
import ua.mykyta_varenyk.hibernate.entity.Role;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleDao roleDao;

    @Autowired
    public RoleServiceImpl(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    @Transactional
    public Role create(Role role) {
        roleDao.save(role);
        return role;
    }

    @Override
    @Transactional(readOnly = true)
    public Role get(String roleName) {
        return roleDao.get(roleName);
    }

    @Override
    @Transactional
    public Role update(Role role) {
        Role persistentRole = roleDao.get(role.getId());

        persistentRole.setRoleName(role.getRoleName());

        return persistentRole;
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        roleDao.delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Role> getAll() {
        return roleDao.getAll();
    }
}
