package ua.mykyta_varenyk.hibernate.service;

import org.springframework.transaction.annotation.Transactional;
import ua.mykyta_varenyk.hibernate.dto.UserDto;
import ua.mykyta_varenyk.hibernate.entity.User;

import java.util.List;
import java.util.UUID;

public interface UserService {
    UserDto create(UserDto dto);

    @Transactional(readOnly = true)
    UserDto get(String login);

    @Transactional
    UserDto update(UserDto dto);

    void delete(UUID id);

    List<User> getAll();
}
