package ua.mykyta_varenyk.hibernate;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.hibernate.SessionFactory;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public abstract class AbstractBaseSpringTest {

    @Configuration
    @ComponentScan(basePackages = "ua.mykyta_varenyk.hibernate")
    @EnableTransactionManagement
    protected static class TestContextConfiguration {
        @Bean
        public DataSource dataSource() throws SQLException {
            MysqlDataSource dataSource = new MysqlDataSource();
            dataSource.setURL("jdbc:mysql://localhost:3306/homework_10_12");
            dataSource.setUser("root");
            dataSource.setPassword("15092023Mykyta");
            dataSource.setServerTimezone("UTC");
            return dataSource;
        }

        @Bean
        public LocalSessionFactoryBean sessionFactory() throws SQLException {
            LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
            sessionFactory.setDataSource(dataSource());
            sessionFactory.setPackagesToScan("ua.mykyta_varenyk.hibernate");
            sessionFactory.setHibernateProperties(hibernateProperties());

            return sessionFactory;
        }

        @Bean
        public PlatformTransactionManager hibernateTransactionManager(SessionFactory sessionFactory) {
            HibernateTransactionManager transactionManager = new HibernateTransactionManager();
            transactionManager.setSessionFactory(sessionFactory);
            return transactionManager;
        }

        private Properties hibernateProperties() {
            Properties hibernateProperties = new Properties();
            hibernateProperties.setProperty("hibernate.hbm2ddl.auto", "update");
            hibernateProperties.setProperty("hibernate.show_sql", "true");
            hibernateProperties.setProperty("hibernate.format_sql", "true");
            hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL8Dialect");

            return hibernateProperties;
        }
    }
}
