package ua.mykytavarenyk.entityinheritance.jointable.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "bank_account_join")
public class BankAccount extends BillingDetails {
    private String account;

    @Column(name = "band_name")
    private String bandName;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBandName() {
        return bandName;
    }

    public void setBandName(String bandName) {
        this.bandName = bandName;
    }
}
