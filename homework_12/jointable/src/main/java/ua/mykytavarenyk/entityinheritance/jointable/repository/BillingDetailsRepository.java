package ua.mykytavarenyk.entityinheritance.jointable.repository;

import ua.mykytavarenyk.entityinheritance.jointable.domain.BillingDetails;

import java.util.List;

public interface BillingDetailsRepository {
    List<BillingDetails> getBillingDetailsOfABuyerById(Long id);
}
