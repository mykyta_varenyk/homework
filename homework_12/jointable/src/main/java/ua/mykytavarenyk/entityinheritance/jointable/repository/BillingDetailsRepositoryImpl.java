package ua.mykytavarenyk.entityinheritance.jointable.repository;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.mykytavarenyk.entityinheritance.jointable.domain.BillingDetails;

import java.util.List;

@Repository
public class BillingDetailsRepositoryImpl implements BillingDetailsRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public BillingDetailsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<BillingDetails> getBillingDetailsOfABuyerById(Long id) {
        return sessionFactory.getCurrentSession().getNamedQuery("billingDetailsByBuyerId").setParameter("id", id).getResultList();
    }
}

