package ua.mykytavarenyk.entityinheritance.jointable.repository;

import ua.mykytavarenyk.entityinheritance.jointable.domain.Buyer;

public interface BuyerRepository {
    Buyer get(String firstName);
}
