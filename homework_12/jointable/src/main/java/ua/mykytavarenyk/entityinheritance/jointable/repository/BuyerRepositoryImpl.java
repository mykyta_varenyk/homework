package ua.mykytavarenyk.entityinheritance.jointable.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.mykytavarenyk.entityinheritance.jointable.domain.Buyer;

@Repository
public class BuyerRepositoryImpl implements BuyerRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public BuyerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Buyer get(String firstName) {
        Session session = sessionFactory.getCurrentSession();

        Object singleResult = session
                .getNamedQuery("buyerByFirstName")
                .setParameter("firstName", firstName)
                .getSingleResult();

        return (Buyer) singleResult;
    }
}
