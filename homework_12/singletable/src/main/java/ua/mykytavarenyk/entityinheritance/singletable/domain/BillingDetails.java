package ua.mykytavarenyk.entityinheritance.singletable.domain;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "billing_details_type")
@NamedQueries(@NamedQuery(name = "billingDetailsByBuyerId",
        query = "from BillingDetails bd where bd.buyer.id=:id"))
public abstract class BillingDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Buyer buyer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public void setBuyer(Buyer buyer) {
        this.buyer = buyer;
    }

    @Override
    public String toString() {
        return "BillingDetails{" +
                "id=" + id +
                ", buyer=" + buyer +
                '}';
    }
}
