package ua.mykytavarenyk.entityinheritance.singletable.repository;

import ua.mykytavarenyk.entityinheritance.singletable.domain.BillingDetails;

import java.util.List;

public interface BillingDetailsRepository {
    List<BillingDetails> getBillingDetailsOfABuyerById(Long id);
}
