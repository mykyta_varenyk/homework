package ua.mykytavarenyk.entityinheritance.singletable.repository;

import ua.mykytavarenyk.entityinheritance.singletable.domain.Buyer;

public interface BuyerRepository {
    Buyer get(String firstName);
}
