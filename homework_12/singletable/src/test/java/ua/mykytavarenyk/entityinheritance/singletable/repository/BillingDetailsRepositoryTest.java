package ua.mykytavarenyk.entityinheritance.singletable.repository;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ua.mykytavarenyk.entityinheritance.singletable.domain.BankAccount;
import ua.mykytavarenyk.entityinheritance.singletable.domain.Buyer;
import ua.mykytavarenyk.entityinheritance.singletable.domain.CreditCard;
import ua.mykytavarenyk.entityinheritance.singletable.repository.configuration.HibernateConfiguration;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = HibernateConfiguration.class)
public class BillingDetailsRepositoryTest {
    @Autowired
    private BillingDetailsRepository billingDetailsRepository;

    @Autowired
    private BuyerRepository buyerRepository;

    @Autowired
    private SessionFactory sessionFactory;

    private Buyer createdBuyer;

    @Before
    public void initializeBuyerAndBillingDetails() {
        Buyer buyer = new Buyer();
        buyer.setFirstName("Mykyta");
        buyer.setLastName("Varenyk");
        sessionFactory.getCurrentSession().save(buyer);

        createdBuyer = buyerRepository.get(buyer.getFirstName());
    }

    @Test
    @Transactional
    public void shouldReturnAllBillingDetailsForBuyerById() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBuyer(createdBuyer);
        bankAccount.setAccount("account");
        bankAccount.setBandName("bandName");

        CreditCard creditCard = new CreditCard();
        creditCard.setCardNumber("333");
        creditCard.setExpMonth(7);
        creditCard.setExpYear(2023);
        creditCard.setBuyer(createdBuyer);

        sessionFactory.getCurrentSession().save(bankAccount);
        sessionFactory.getCurrentSession().save(creditCard);

        assertEquals(2, billingDetailsRepository.getBillingDetailsOfABuyerById(createdBuyer.getId()).size());
    }

}