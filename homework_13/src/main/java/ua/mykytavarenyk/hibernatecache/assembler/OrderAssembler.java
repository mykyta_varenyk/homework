package ua.mykytavarenyk.hibernatecache.assembler;

import ua.mykytavarenyk.hibernatecache.dto.OrderDto;
import ua.mykytavarenyk.hibernatecache.entity.Order;

public interface OrderAssembler {
    Order assemble(OrderDto orderDto);

    OrderDto assemble(Order entity);
}
