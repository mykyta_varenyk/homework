package ua.mykytavarenyk.hibernatecache.assembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.mykytavarenyk.hibernatecache.dao.UserDao;
import ua.mykytavarenyk.hibernatecache.dto.OrderDto;
import ua.mykytavarenyk.hibernatecache.entity.Order;

@Service
public class OrderAssemblerImpl implements OrderAssembler {
    private final UserDao userDao;

    @Autowired
    public OrderAssemblerImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public Order assemble(OrderDto orderDto) {
        Order order = new Order();
        order.setUser(userDao.get(orderDto.getUserId()));
        order.setId(orderDto.getId());
        order.setBookName(orderDto.getBookName());
        return order;
    }

    @Override
    public OrderDto assemble(Order entity) {
        OrderDto orderDto = new OrderDto();

        orderDto.setId(entity.getId());

        orderDto.setUserId(entity.getUser().getId());

        orderDto.setBookName(entity.getBookName());

        return orderDto;
    }
}

