package ua.mykytavarenyk.hibernatecache.assembler;

import ua.mykytavarenyk.hibernatecache.dto.UserDto;
import ua.mykytavarenyk.hibernatecache.entity.User;

public interface UserAssembler {
    User assemble(UserDto userDto);

    UserDto assemble(User entity);
}
