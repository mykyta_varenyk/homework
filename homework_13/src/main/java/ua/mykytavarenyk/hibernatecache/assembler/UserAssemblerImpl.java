package ua.mykytavarenyk.hibernatecache.assembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.mykytavarenyk.hibernatecache.dao.RoleDao;
import ua.mykytavarenyk.hibernatecache.dao.UserDao;
import ua.mykytavarenyk.hibernatecache.dto.OrderDto;
import ua.mykytavarenyk.hibernatecache.dto.UserDto;
import ua.mykytavarenyk.hibernatecache.entity.Order;
import ua.mykytavarenyk.hibernatecache.entity.Role;
import ua.mykytavarenyk.hibernatecache.entity.User;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserAssemblerImpl implements UserAssembler {

    private final RoleDao roleDao;

    @Autowired
    public UserAssemblerImpl(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public User assemble(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setName(userDto.getName());
        user.setLogin(userDto.getLogin());
        user.setRole(roleDao.get(userDto.getRoleId()));

        if (userDto.getOrders() != null) {
            for (OrderDto orderDto : userDto.getOrders()) {
                Order order = assemble(orderDto);

                Role role = roleDao.get(userDto.getRoleId());

                user.setRole(role);

                user.getOrders().add(order);
            }
        }

        return user;
    }

    @Override
    public UserDto assemble(User entity) {
        UserDto userDto = new UserDto();

        userDto.setId(entity.getId());

        userDto.setName(entity.getName());

        userDto.setLogin(entity.getLogin());

        userDto.setRoleId(entity.getRole().getId());

        List<OrderDto> orders = entity.getOrders()
                .stream()
                .map(this::assemble)
                .collect(Collectors.toList());

        userDto.setOrders(orders);

        return userDto;
    }

    private Order assemble(OrderDto dto) {
        Order order = new Order();
        order.setBookName(dto.getBookName());
        return order;
    }

    private OrderDto assemble(Order order) {
        OrderDto dto = new OrderDto();
        User user = new User();
        user.setId(order.getId());
        order.setUser(user);
        dto.setBookName(order.getBookName());
        return dto;
    }

}
