package ua.mykytavarenyk.hibernatecache.dao;

import org.springframework.transaction.annotation.Transactional;
import ua.mykytavarenyk.hibernatecache.entity.Order;

import java.util.List;
import java.util.UUID;

public interface OrderDao {
    @Transactional
    void save(Order order);

    Order get(UUID orderId);

    Order get(String bookName);

    void delete(UUID orderId);

    List<Order> getAll();
}
