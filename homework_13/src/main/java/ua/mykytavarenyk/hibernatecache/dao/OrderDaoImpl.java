package ua.mykytavarenyk.hibernatecache.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.mykytavarenyk.hibernatecache.entity.Order;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Repository
public class OrderDaoImpl implements OrderDao {
    private final SessionFactory sessionFactory;

    @Autowired
    public OrderDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public void save(Order order) {
        Session session = sessionFactory.getCurrentSession();

        session.save(order);
    }

    @Override
    public Order get(UUID orderId) {
        Session session = sessionFactory.getCurrentSession();

        Order order = session.get(Order.class, orderId);

        return Objects.requireNonNull(order, "Order not found by id:" + orderId);
    }

    @Override
    public Order get(String bookName) {
        Session session = sessionFactory.getCurrentSession();

        Object singleResult = session
                .getNamedQuery("orderByBookName")
                .setParameter("bookName", bookName)
                .getSingleResult();

        return (Order) singleResult;
    }

    @Override
    public void delete(UUID orderId) {
        Order order = get(orderId);

        sessionFactory.getCurrentSession().delete(order);
    }

    @Override
    public List<Order> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Order").list();
    }
}
