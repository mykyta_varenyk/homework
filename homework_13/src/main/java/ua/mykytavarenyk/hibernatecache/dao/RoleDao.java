package ua.mykytavarenyk.hibernatecache.dao;

import ua.mykytavarenyk.hibernatecache.entity.Role;

import java.util.List;

public interface RoleDao {
    Role create(String roleName);

    void save(Role role);

    Role get(Integer roleId);

    Role get(String roleName);

    void delete(Integer id);

    List<Role> getAll();
}
