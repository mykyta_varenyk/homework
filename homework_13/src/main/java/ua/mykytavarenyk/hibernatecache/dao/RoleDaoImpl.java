package ua.mykytavarenyk.hibernatecache.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.mykytavarenyk.hibernatecache.entity.Role;

import java.util.List;
import java.util.Objects;

@Repository
public class RoleDaoImpl implements RoleDao {
    private final SessionFactory sessionFactory;

    @Autowired
    public RoleDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Role create(String roleName) {
        Role role = new Role();

        role.setRoleName(roleName);

        sessionFactory.getCurrentSession().save(role);

        return role;
    }

    @Override
    public void save(Role role) {
        Session session = sessionFactory.getCurrentSession();

        session.save(role);
    }

    @Override
    public Role get(Integer roleId) {
        Session session = sessionFactory.getCurrentSession();

        Role role = session.get(Role.class, roleId);

        return Objects.requireNonNull(role, "Role not found by id: " + roleId);
    }

    @Override
    public Role get(String roleName) {
        Session session = sessionFactory.getCurrentSession();

        Object singleResult = session
                .getNamedQuery("roleByRoleName")
                .setParameter("roleName", roleName)
                .getSingleResult();

        return (Role) singleResult;
    }

    @Override
    public void delete(Integer id) {
        Role role = get(id);

        sessionFactory.getCurrentSession().delete(role);
    }

    @Override
    public List<Role> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Role").list();
    }
}
