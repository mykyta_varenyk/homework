package ua.mykytavarenyk.hibernatecache.dao;

import ua.mykytavarenyk.hibernatecache.entity.User;

import java.util.List;
import java.util.UUID;

public interface UserDao {
    void save(User user);

    User get(UUID userId);

    User get(String login);

    void delete(UUID id);

    List<User> getAll();
}
