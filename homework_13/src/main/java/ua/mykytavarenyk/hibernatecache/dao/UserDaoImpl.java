package ua.mykytavarenyk.hibernatecache.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.mykytavarenyk.hibernatecache.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Repository
public class UserDaoImpl implements UserDao {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(User user) {
        Session session = sessionFactory.getCurrentSession();

        session.save(user);
    }

    @Override
    public User get(UUID userId) {
        Session session = sessionFactory.getCurrentSession();

        User user = session.get(User.class, userId);

        return Objects.requireNonNull(user, "User not found by id: " + userId);
    }

    @Override
    public User get(String login) {
        Session session = sessionFactory.getCurrentSession();

        Object singleResult = session
                .getNamedQuery("userByLogin")
                .setParameter("login", login)
                .getSingleResult();

        return (User) singleResult;
    }

    @Override
    public void delete(UUID id) {
        User user = get(id);

        sessionFactory.getCurrentSession().delete(user);
    }

    @Override
    public List<User> getAll() {
        List<User> users = sessionFactory.getCurrentSession().createQuery("from User").list();
        return users == null ? new ArrayList<>() : users;
    }
}
