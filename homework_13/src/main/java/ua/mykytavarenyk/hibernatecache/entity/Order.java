package ua.mykytavarenyk.hibernatecache.entity;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "order_entity")
@NamedQueries(@NamedQuery(name = "orderByBookName",
        query = "from Order o where o.bookName=:bookName "))
public class Order {
    @Id
    @Column(length = 16)
    @GeneratedValue
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    private String bookName;

    @ManyToOne
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
}
