package ua.mykytavarenyk.hibernatecache.service;

import org.springframework.transaction.annotation.Transactional;
import ua.mykytavarenyk.hibernatecache.dto.OrderDto;
import ua.mykytavarenyk.hibernatecache.entity.Order;

import java.util.List;
import java.util.UUID;

public interface OrderService {
    @Transactional
    OrderDto create(OrderDto dto);

    @Transactional(readOnly = true)
    OrderDto get(String bookName);

    @Transactional(readOnly = true)
    OrderDto get(UUID id);

    @Transactional
    OrderDto update(OrderDto dto);

    @Transactional(readOnly = true)
    List<Order> getAll();

    @Transactional
    void delete(UUID id);
}
