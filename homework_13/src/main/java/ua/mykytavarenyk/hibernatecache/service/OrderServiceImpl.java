package ua.mykytavarenyk.hibernatecache.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.mykytavarenyk.hibernatecache.assembler.OrderAssembler;
import ua.mykytavarenyk.hibernatecache.dao.OrderDao;
import ua.mykytavarenyk.hibernatecache.dto.OrderDto;
import ua.mykytavarenyk.hibernatecache.entity.Order;

import java.util.List;
import java.util.UUID;

@Service
public class OrderServiceImpl implements OrderService {
    private final OrderDao orderDao;

    private final OrderAssembler orderAssembler;

    @Autowired
    public OrderServiceImpl(OrderDao orderDao, OrderAssembler orderAssembler) {
        this.orderDao = orderDao;

        this.orderAssembler = orderAssembler;
    }

    @Override
    @Transactional
    public OrderDto create(OrderDto dto) {
        Order order = orderAssembler.assemble(dto);

        orderDao.save(order);

        return orderAssembler.assemble(order);
    }

    @Override
    @Transactional(readOnly = true)
    public OrderDto get(String bookName) {
        Order order = orderDao.get(bookName);
        return orderAssembler.assemble(order);
    }

    @Override
    @Transactional(readOnly = true)
    public OrderDto get(UUID id) {
        Order order = orderDao.get(id);
        return orderAssembler.assemble(order);
    }

    @Override
    @Transactional
    public OrderDto update(OrderDto dto) {
        Order entity = orderDao.get(dto.getId());

        Order updatedEntity = orderAssembler.assemble(dto);

        performUpdate(entity, updatedEntity);

        return orderAssembler.assemble(entity);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Order> getAll() {
        return orderDao.getAll();
    }

    private void performUpdate(Order persistentEntity, Order updatedEntity) {
        persistentEntity.setUser(updatedEntity.getUser());
        persistentEntity.setBookName(updatedEntity.getBookName());
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        orderDao.delete(id);
    }
}
