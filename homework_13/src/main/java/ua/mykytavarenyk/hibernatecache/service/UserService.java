package ua.mykytavarenyk.hibernatecache.service;

import org.springframework.transaction.annotation.Transactional;
import ua.mykytavarenyk.hibernatecache.dto.UserDto;
import ua.mykytavarenyk.hibernatecache.entity.User;

import java.util.List;
import java.util.UUID;

public interface UserService {
    @Transactional
    UserDto create(UserDto dto);

    @Transactional(readOnly = true)
    UserDto get(String login);

    @Transactional
    UserDto update(UserDto dto);

    @Transactional
    void delete(UUID id);

    @Transactional(readOnly = true)
    List<User> getAll();
}
