package ua.mykytavarenyk.hibernatecache.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.mykytavarenyk.hibernatecache.assembler.UserAssembler;
import ua.mykytavarenyk.hibernatecache.dao.UserDao;
import ua.mykytavarenyk.hibernatecache.dto.UserDto;
import ua.mykytavarenyk.hibernatecache.entity.Order;
import ua.mykytavarenyk.hibernatecache.entity.User;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    private final UserDao userDao;

    private final UserAssembler userAssembler;

    @Autowired
    public UserServiceImpl(UserDao userDao, UserAssembler userAssembler) {
        this.userDao = userDao;
        this.userAssembler = userAssembler;
    }

    @Override
    @Transactional
    public UserDto create(UserDto dto) {
        User user = userAssembler.assemble(dto);

        userDao.save(user);

        return userAssembler.assemble(user);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDto get(String login) {
        User user = userDao.get(login);
        return userAssembler.assemble(user);
    }

    @Override
    @Transactional
    public UserDto update(UserDto dto) {
        User entity = userDao.get(dto.getId());

        User updatedEntity = userAssembler.assemble(dto);

        performUpdate(entity, updatedEntity);

        return userAssembler.assemble(entity);
    }

    private void performUpdate(User persistentEntity, User newEntity) {
        persistentEntity.setName(newEntity.getName());
        persistentEntity.setLogin(newEntity.getLogin());
        updateBooks(persistentEntity.getOrders(), newEntity.getOrders());
    }

    private void updateBooks(List<Order> persistentOrders, List<Order> newOrders) {
        Map<UUID, Order> stillExistentBooks = new HashMap<>();

        List<Order> booksToAdd = new ArrayList<>();

        if (!persistentOrders.isEmpty()) {
            for (Order o1 : newOrders) {
                for (Order o2 : persistentOrders) {
                    if (o1.getId() != o2.getId()) {
                        booksToAdd.add(o1);
                    } else {
                        stillExistentBooks.put(o2.getId(), o2);
                    }
                }
            }
        } else {
            booksToAdd.addAll(newOrders);
        }

        Iterator<Order> persistentIterator = persistentOrders.iterator();

        while (persistentIterator.hasNext()) {
            Order persistentOrder = persistentIterator.next();

            if (stillExistentBooks.containsKey(persistentOrder.getId())) {
                Order updatedBook = stillExistentBooks.get(persistentOrder.getId());
                updateOrder(persistentOrder, updatedBook);
            } else {
                persistentIterator.remove();
                persistentOrder.setUser(null);
            }
        }
        persistentOrders.addAll(booksToAdd);
    }

    private void updateOrder(Order persistentOrder, Order updatedOrder) {
        persistentOrder.setBookName(updatedOrder.getBookName());
        persistentOrder.setUser(updatedOrder.getUser());
    }

    @Transactional
    @Override
    public void delete(UUID id) {
        userDao.delete(id);
    }

    @Transactional(readOnly = true)
    @Override
    public List<User> getAll() {
        return userDao.getAll();
    }

}
