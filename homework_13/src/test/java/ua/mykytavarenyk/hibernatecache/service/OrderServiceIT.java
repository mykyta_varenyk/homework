package ua.mykytavarenyk.hibernatecache.service;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ua.mykytavarenyk.hibernatecache.dao.RoleDao;
import ua.mykytavarenyk.hibernatecache.dao.UserDao;
import ua.mykytavarenyk.hibernatecache.dto.OrderDto;
import ua.mykytavarenyk.hibernatecache.entity.Order;
import ua.mykytavarenyk.hibernatecache.entity.Role;
import ua.mykytavarenyk.hibernatecache.entity.User;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class OrderServiceIT extends AbstractBaseSpringTest {
    @Autowired
    private OrderService orderService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    private User user;

    private static final String[] ROLE_NAMES = {"USER", "LIBRARIAN", "ADMIN"};

    private Map<String, Role> roles = Collections.emptyMap();

    @Before
    public void init() {
        List<Role> initialRoles = roleDao.getAll();
        if (initialRoles.isEmpty()) {
            initialRoles = createRoles();
        }

        roles = initialRoles.stream().collect(Collectors.toMap(Role::getRoleName, Function.identity()));
        user = new User();
        user.setName("name");
        user.setLogin("login");
        Role role = new Role();
        role.setId(getRoleId(0));
        user.setRole(role);
        userDao.save(user);
    }

    private List<Role> createRoles() {
        return Stream
                .of(ROLE_NAMES)
                .map(name -> roleDao.create(name))
                .collect(Collectors.toList());
    }

    @Test
    @Transactional
    public void shouldCreateOrder() {
        UUID userId = user.getId();
        OrderDto dto = createOrderDto(userId);

        OrderDto createdOrder = orderService.create(dto);

        assertEquals(dto.getUserId(), createdOrder.getUserId());
    }

    @Test
    @Transactional
    public void shouldGetOrder() {
        UUID userId = user.getId();
        OrderDto dto = createOrderDto(userId);
        OrderDto createdOrder = orderService.create(dto);

        OrderDto result = orderService.get(createdOrder.getBookName());

        assertEquals(dto.getBookName(), result.getBookName());
    }

    @Test
    @Transactional
    public void shouldUpdateOrder() {
        UUID userId = user.getId();
        OrderDto dto = createOrderDto(userId);
        OrderDto createdOrder = orderService.create(dto);

        createdOrder.setBookName("The Chronicles of Narnia");

        OrderDto updatedOrder = orderService.update(createdOrder);

        assertEquals(createdOrder.getBookName(), updatedOrder.getBookName());
    }

    @Test
    @Transactional
    public void shouldDeleteOrder() {
        List<Order> orders = orderService.getAll();
        UUID userId = user.getId();
        OrderDto dto = createOrderDto(userId);
        OrderDto createdOrder = orderService.create(dto);

        orderService.delete(createdOrder.getId());

        assertEquals(orders.size(), orderService.getAll().size());
    }

    private OrderDto createOrderDto(UUID userId) {
        OrderDto dto = new OrderDto();
        dto.setBookName("The Lord of The Rings");
        dto.setUserId(userId);
        return dto;
    }

    private Integer getRoleId(int roleIndex) {
        return roles.get(ROLE_NAMES[roleIndex]).getId();
    }
}