package ua.mykytavarenyk.hibernatecache.service;

import net.bytebuddy.utility.RandomString;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ua.mykytavarenyk.hibernatecache.entity.Role;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class RoleServiceIT extends AbstractBaseSpringTest {
    @Autowired
    private RoleService roleService;

    @Autowired
    private SessionFactory sessionFactory;

    @Test
    @Transactional
    public void shouldCreateRole() {
        Role role = createRole(RandomString.make(7));

        Role createdRole = roleService.create(role);

        assertEquals(role.getRoleName(), createdRole.getRoleName());
    }

    @Test
    @Transactional
    public void shouldGetRole() {
        String roleName = RandomString.make(7);

        Role role = createRole(roleName);

        roleService.create(role);

        Role result = roleService.get(roleName);

        assertEquals(role.getRoleName(), result.getRoleName());
    }

    @Test
    @Transactional
    public void shouldUpdateRole() {
        String roleName = RandomString.make(7);
        Role role = createRole(roleName);
        Role createdRole = roleService.create(role);

        createdRole.setRoleName(RandomString.make(7));
        Role updatedRole = roleService.update(createdRole);

        assertEquals(createdRole.getRoleName(), updatedRole.getRoleName());
    }

    @Test
    @Transactional
    public void shouldDeleteRole() {
        List<Role> roles = roleService.getAll();
        String roleName = RandomString.make(7);
        Role role = createRole(roleName);
        Role createdRole = roleService.create(role);

        roleService.delete(createdRole.getId());

        assertEquals(roles.size(), roleService.getAll().size());
    }

    private Role createRole(String roleName) {
        Role role = new Role();
        role.setId(1);
        role.setRoleName(roleName);
        return role;
    }
}
