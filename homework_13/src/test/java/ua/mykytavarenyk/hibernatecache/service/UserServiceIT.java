package ua.mykytavarenyk.hibernatecache.service;

import net.bytebuddy.utility.RandomString;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ua.mykytavarenyk.hibernatecache.dao.RoleDao;
import ua.mykytavarenyk.hibernatecache.dao.UserDao;
import ua.mykytavarenyk.hibernatecache.dto.OrderDto;
import ua.mykytavarenyk.hibernatecache.dto.UserDto;
import ua.mykytavarenyk.hibernatecache.entity.Role;
import ua.mykytavarenyk.hibernatecache.entity.User;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class UserServiceIT extends AbstractBaseSpringTest {
    @Autowired
    private UserService userService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    private static final String[] ROLE_NAMES = {"USER", "LIBRARIAN", "ADMIN"};

    private Map<String, Role> roles = Collections.emptyMap();

    @Before
    public void initializeRoles() {
        List<Role> initialRoles = roleDao.getAll();
        if (initialRoles.isEmpty()) {
            initialRoles = createRoles();
        }
        roles = initialRoles.stream().collect(Collectors.toMap(Role::getRoleName, Function.identity()));
    }

    private List<Role> createRoles() {
        return Stream
                .of(ROLE_NAMES)
                .map(name -> roleDao.create(name))
                .collect(Collectors.toList());
    }

    @Test
    @Transactional
    public void shouldCreateAUser() {
        UserDto dto = createUserDto("mykytavar");

        UserDto createdUser = userService.create(dto);

        assertEquals(dto.getOrders().size(), createdUser.getOrders().size());
    }

    private UserDto createUserDto(String login) {
        UserDto dto = new UserDto();
        dto.setLogin(login);
        dto.setName("Mykyta Varenyk");
        dto.setRoleId(getRoleId(1));
        return dto;
    }

    @Test
    @Transactional
    public void shouldGetUser() {
        String login = RandomString.make(7);
        UserDto dto = createUserDto(login);

        userService.create(dto);
        UserDto result = userService.get(login);

        assertEquals(dto.getLogin(), result.getLogin());
    }

    @Test
    @Transactional
    public void shouldUpdateUser() {
        String login = RandomString.make(7);
        UserDto userDto = createUserDto(login);
        UserDto createdUser = userService.create(userDto);

        OrderDto firstOrder = new OrderDto();
        firstOrder.setBookName("The Lord of The Rings");
        firstOrder.setUserId(createdUser.getId());
        createdUser.getOrders().add(firstOrder);
        UserDto updated = userService.update(createdUser);

        assertEquals(createdUser.getOrders().get(0).getId(), updated.getOrders().get(0).getId());
    }

    @Test
    @Transactional
    public void shouldDeleteUser() {
        List<User> users = userService.getAll();
        String login = RandomString.make(7);
        UserDto dto = createUserDto(login);
        UserDto createdUser = userService.create(dto);

        userService.delete(createdUser.getId());

        assertEquals(users.size(), userDao.getAll().size());
    }

    private Integer getRoleId(int roleIndex) {
        return roles.get(ROLE_NAMES[roleIndex]).getId();
    }
}