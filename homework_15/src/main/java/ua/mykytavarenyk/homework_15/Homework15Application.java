package ua.mykytavarenyk.homework_15;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication

public class Homework15Application {

    @Bean
    public CommandLineRunner tutorial() {
        return new RabbitAmpqRunner();
    }

    public static void main(String[] args) {
        SpringApplication.run(Homework15Application.class, args);
    }
}
