package ua.mykytavarenyk.homework_15;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ConfigurableApplicationContext;
import ua.mykytavarenyk.homework_15.correlationidpattern.MessageSender;

public class RabbitAmpqRunner implements CommandLineRunner {
    @Autowired
    private ConfigurableApplicationContext context;

    @Override
    public void run(String... args) throws Exception {
        MessageSender sender = new MessageSender();

        sender.send();

        context.close();
    }
}
