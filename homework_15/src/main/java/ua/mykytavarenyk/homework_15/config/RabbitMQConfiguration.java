package ua.mykytavarenyk.homework_15.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ua.mykytavarenyk.homework_15.correlationidpattern.MessageSender;
import ua.mykytavarenyk.homework_15.correlationidpattern.MessageServer;

@Configuration
@Profile({"homework_15", "pipe"})
public class RabbitMQConfiguration {

    @Bean
    public RabbitTemplate template() {
        return new RabbitTemplate();
    }

    @Profile("client")
    private static class ClientConfig {
        @Bean
        public DirectExchange exchange() {
            return new DirectExchange("homework15.pipe");
        }

        @Bean
        public MessageSender client() {
            return new MessageSender();
        }

        @Bean
        public DirectExchange direct() {
            return new DirectExchange("homework15.pipe.responses");
        }

        @Bean
        public Queue queueForResponses() {
            return new Queue("homework15.pipe.requests");
        }

        @Bean
        public Binding binding(DirectExchange direct, Queue queueForResponses) {
            return BindingBuilder.bind(queueForResponses)
                    .to(direct)
                    .with("response");
        }
    }

    @Profile("server")
    private static class ServerConfig {
        @Bean
        public Queue queue() {
            return new Queue("homework15.pipe.requests");
        }

        @Bean
        public DirectExchange exchange() {
            return new DirectExchange("homework15.pipe");
        }

        @Bean
        public Binding binding(DirectExchange exchange,
                               Queue queue) {
            return BindingBuilder.bind(queue)
                    .to(exchange)
                    .with("pipe");
        }

        @Bean
        public MessageServer server() {
            return new MessageServer();
        }

    }

}
