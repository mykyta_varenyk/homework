package ua.mykytavarenyk.homework_15.correlationidpattern;

import java.io.Serializable;
import java.util.UUID;

public class Message implements Serializable {
    private int firstOperand;

    private int secondOperand;

    private UUID correlationId = UUID.randomUUID();

    public Message(int firstOperand, int secondOperand) {
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
    }

    public int getFirstOperand() {
        return firstOperand;
    }

    public void setFirstOperand(int firstOperand) {
        this.firstOperand = firstOperand;
    }

    public int getSecondOperand() {
        return secondOperand;
    }

    public void setSecondOperand(int secondOperand) {
        this.secondOperand = secondOperand;
    }

    public UUID getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(UUID correlationId) {
        this.correlationId = correlationId;
    }

    @Override
    public String toString() {
        return "Message{" +
                "firstOperand=" + firstOperand +
                ", secondOperand=" + secondOperand +
                '}';
    }
}
