package ua.mykytavarenyk.homework_15.correlationidpattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.*;

@Service
public class MessageSender {
    @Autowired
    private RabbitTemplate template;

    private static final Logger log = LoggerFactory.getLogger(MessageSender.class);

   /* @Autowired
    private DirectExchange exchange;
    */

    private final Random random = new SecureRandom();

    private Map<UUID, Message> requests = new HashMap<>();

    @Scheduled(fixedDelay = 300)
    public void send() {
        for (int i = 0; i < 10; i++) {
            Message message = new Message(random.nextInt(10), random.nextInt(10));

            requests.put(message.getCorrelationId(), message);

            template.convertAndSend(message);
        }
    }

    @RabbitListener(queues = "homework15.pipe.responses")
    public void receiveReply(@Payload Response response) {
        if (!requests.containsKey(Objects.requireNonNull(response).getCorrelationId())) {
            log.debug("No such correlationId");
        }
    }

}