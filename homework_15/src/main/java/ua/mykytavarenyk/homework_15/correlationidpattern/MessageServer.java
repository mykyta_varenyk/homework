package ua.mykytavarenyk.homework_15.correlationidpattern;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MessageServer {
    private List<Message> listOfRequests = new LinkedList<>();

    @Autowired
    private RabbitTemplate template;

    @RabbitListener(queues = "homework15.pipe.requests")
    public void sum(Message message) {
        System.out.println("Received requests from " + message.toString());

        listOfRequests.add(message);

        if (listOfRequests.size() >= 10) {
            for (Message currentMessage : listOfRequests) {
                int result = currentMessage.getFirstOperand() + currentMessage.getSecondOperand();

                System.out.println("Returned " + result);

                template.convertAndSend(new Response(result, currentMessage.getCorrelationId()));
            }
        }
    }
}
