package ua.mykytavarenyk.homework_15.correlationidpattern;

import java.util.UUID;

public class Response {
    private int operationResult;

    private UUID correlationId;

    public int getOperationResult() {
        return operationResult;
    }

    public void setOperationResult(int operationResult) {
        this.operationResult = operationResult;
    }

    public UUID getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(UUID correlationId) {
        this.correlationId = correlationId;
    }

    public Response(int operationResult, UUID correlationId) {
        this.operationResult = operationResult;
        this.correlationId = correlationId;
    }

    @Override
    public String toString() {
        return "Response{" +
                "operationResult=" + operationResult +
                ", correlationId=" + correlationId +
                '}';
    }
}
