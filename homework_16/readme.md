# Homework 16

HW Create this infrastructure https://hw-rd-bucket-temp.s3.amazonaws.com/hw-infrastructure.png
using cloudformation and web console or aws cli Create simple java project that has CRUD operations for simple entity in
RDS. Put jar with your pet project into S3 to make it accessable for EC2 user data script.

AC:
Your cloudformation template should create ELB, target group with 2 EC2s. If some services require payment, you can skip
them.

# Note

I have created two cloudformation templates: the first one contains all the resources from schema, and the second one
only those which are mentioned in the AC. Unfortunately, having created the required infrastructure using aws web
console, when starting the system, I get 502 code.