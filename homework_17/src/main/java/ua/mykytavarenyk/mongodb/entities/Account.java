package ua.mykytavarenyk.mongodb.entities;

import java.time.LocalDate;
import java.util.UUID;

public class Account {
    private UUID cardNumber;

    private String nameOfAccount;

    private LocalDate expirationDate;

    public Account(UUID cardNumber, String nameOfAccount, LocalDate expirationDate) {
        this.cardNumber = cardNumber;
        this.nameOfAccount = nameOfAccount;
        this.expirationDate = expirationDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public UUID getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(UUID cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getNameOfAccount() {
        return nameOfAccount;
    }

    public void setNameOfAccount(String nameOfAccount) {
        this.nameOfAccount = nameOfAccount;
    }
}
