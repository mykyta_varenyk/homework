package ua.mykytavarenyk.mongodb.entities;

import java.util.Objects;

public class Address {
    private String line1;

    private String line2;

    private String countryCode;

    public Address(String line1, String line2, String countryCode) {
        this.line1 = line1;
        this.line2 = line2;
        this.countryCode = countryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(line1, address.line1) && Objects.equals(line2, address.line2) && Objects.equals(countryCode, address.countryCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(line1, line2, countryCode);
    }
}
