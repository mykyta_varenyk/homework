package ua.mykytavarenyk.mongodb.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import ua.mykytavarenyk.mongodb.entities.Address;
import ua.mykytavarenyk.mongodb.entities.Customer;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CustomerRepository extends MongoRepository<Customer,Integer> {
    Optional<List<Customer>> findCustomersByFirstNameAndLastName(String firstName,String lastName);

    Optional<List<Customer>> findCustomersById(BigInteger id);

    Optional<List<Customer>> findCustomersByAddresses(List<Address> addresses);

    @Query("{'accounts.cardNumber' :?0}")
    Optional<List<Customer>> findCustomersByCardNumber(UUID cardNumber);

    @Query("{'accounts.expirationDate' : {$lt: ?0}}")
    Optional<List<Customer>> findCustomersWithExpiredCards(LocalDate localDate);
}
