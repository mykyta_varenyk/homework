package ua.mykytavarenyk.mongodb.services;

import ua.mykytavarenyk.mongodb.entities.Address;
import ua.mykytavarenyk.mongodb.entities.Customer;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CustomerService {
    Optional<List<Customer>> findById(BigInteger id);

    Optional<List<Customer>> findByFirstNameAndLastName(String firstName, String lastName);

    Optional<List<Customer>> findByAddresses(Address address);

    Customer createCustomer(Customer customer);

    Customer updateCustomer(Customer customer);

    Optional<List<Customer>> findCustomersWithExpiredCards();

    Optional<List<Customer>> findCustomersByCardNumber(UUID id);
}
