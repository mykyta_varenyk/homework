package ua.mykytavarenyk.mongodb.services.impl;

import org.springframework.stereotype.Service;
import ua.mykytavarenyk.mongodb.entities.Address;
import ua.mykytavarenyk.mongodb.entities.Customer;
import ua.mykytavarenyk.mongodb.repositories.CustomerRepository;
import ua.mykytavarenyk.mongodb.services.CustomerService;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository repository;

    public CustomerServiceImpl(CustomerRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<List<Customer>> findById(BigInteger id){
        return repository.findCustomersById(id);
    }

    @Override
    public Optional<List<Customer>> findByFirstNameAndLastName(String firstName, String lastName){
        return repository.findCustomersByFirstNameAndLastName(firstName,lastName);
    }

    @Override
    public Optional<List<Customer>> findByAddresses(Address address){
        return repository.findCustomersByAddresses(Collections.singletonList(address));
    }

    @Override
    public Customer createCustomer(Customer customer){
        return repository.insert(customer);
    }

    @Override
    public Customer updateCustomer(Customer customer){
        return repository.save(customer);
    }

    @Override
    public Optional<List<Customer>> findCustomersWithExpiredCards(){
        return repository.findCustomersWithExpiredCards(LocalDate.now());
    }

    public Optional<List<Customer>> findCustomersByCardNumber(UUID id){
        return repository.findCustomersByCardNumber(id);
    }
}
