package ua.mykytavarenyk.mongodb;

import net.bytebuddy.utility.RandomString;
import ua.mykytavarenyk.mongodb.entities.Account;
import ua.mykytavarenyk.mongodb.entities.Address;
import ua.mykytavarenyk.mongodb.entities.Customer;

import java.time.LocalDate;
import java.util.Collections;
import java.util.UUID;

public class GenerateEntity {
    public static Customer generateCustomer(){
        return new Customer(null, RandomString.make(), RandomString.make(),
                Collections.singletonList(new Account(UUID.randomUUID(), "name1", LocalDate.of(2023, 9, 23))),
                Collections.singletonList(new Address(RandomString.make(), RandomString.make(), RandomString.make())),
                null);
    }
}
