package ua.mykytavarenyk.mongodb.services.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ua.mykytavarenyk.mongodb.GenerateEntity;
import ua.mykytavarenyk.mongodb.entities.Customer;
import ua.mykytavarenyk.mongodb.services.CustomerService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class CustomerServiceIT {

    @Autowired
    private CustomerService customerService;

    @Test
    public void shouldCreateCustomer() {
        //Given
        Customer customer = GenerateEntity.generateCustomer();

        //When
        customerService.createCustomer(customer);

        //Then
        assertEquals(customer.getId(), customerService.findById(customer.getId()).get().get(0).getId());
    }

    @Test
    public void shouldUpdateCustomer() {
        //Given
        Customer createdCustomer = customerService.createCustomer(GenerateEntity.generateCustomer());

        //When
        createdCustomer.setFirstName("Daniel");
        customerService.updateCustomer(createdCustomer);

        //Then
        assertEquals("Daniel", customerService.updateCustomer(createdCustomer).getFirstName());
    }

    @Test
    public void shouldFindCustomerById() {
        //Given
        Customer createdCustomer = customerService.createCustomer(GenerateEntity.generateCustomer());

        //When
        Customer foundedUser = customerService.findById(createdCustomer.getId()).get().get(0);

        //Then
        assertEquals(createdCustomer.getId(),foundedUser.getId());
    }

    @Test
    public void shouldFindCustomerByFirstNameAndLastName() {
        //Given
        Customer createdCustomer = customerService.createCustomer(GenerateEntity.generateCustomer());

        //When
        Customer foundedCustomer = customerService.
                findByFirstNameAndLastName(createdCustomer.getFirstName(), createdCustomer.getLastName()).get().get(0);

        //Then
        assertEquals(createdCustomer.getFirstName(), foundedCustomer.getFirstName());
        assertEquals(createdCustomer.getFirstName(), foundedCustomer.getFirstName());
    }

    @Test
    public void shouldFindByAddress() {
        //Given
        Customer createdCustomer = customerService.createCustomer(GenerateEntity.generateCustomer());

        //When
        Customer foundedCustomer = customerService.findByAddresses(createdCustomer.getAddresses().get(0)).get().get(0);

        //Then
        assertEquals(createdCustomer.getAddresses().get(0), foundedCustomer.getAddresses().get(0));
    }

    @Test
    public void shouldFindByCardNumber() {
        //Given
        Customer createdCustomer = customerService.createCustomer(GenerateEntity.generateCustomer());

        //When
        Customer foundedCustomer = customerService.findCustomersByCardNumber(createdCustomer.getAccounts().get(0).getCardNumber()).get().get(0);

        //Then
        assertEquals(createdCustomer.getAccounts().get(0).getCardNumber(), foundedCustomer.getAccounts().get(0).getCardNumber());
    }

    @Test
    public void shouldFindAllCustomersWithExpiredCardNumbers() {
        //Given
        List<Customer> customers = new ArrayList<>();
        customers.add(GenerateEntity.generateCustomer());
        customers.add(GenerateEntity.generateCustomer());

        //When
        customers.get(0).getAccounts().get(0).setExpirationDate(LocalDate.of(2021, 1, 1));
        for (Customer customer : customers) {
            customerService.createCustomer(customer);
        }

        //Then
        assertTrue(customerService.findCustomersWithExpiredCards().get().get(0).getAccounts().get(0).getExpirationDate().isBefore(LocalDate.now()));
    }
}