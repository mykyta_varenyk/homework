package com.mykyta_varenyk.springboot;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;

@Component
public class MyHealthIndicator extends AbstractHealthIndicator {

    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        int random = ThreadLocalRandom.current().nextInt(10);
        builder.up();
        if (random < 3) {
            builder.down();
        }
        builder.withDetail("message", "homework_5");
    }
}
