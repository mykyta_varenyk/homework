package com.mykyta_varenyk.springboot.controller;

import com.mykyta_varenyk.springboot.model.Book;
import com.mykyta_varenyk.springboot.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class BookController {
    @Autowired
    private BookRepository bookRepository;

    @GetMapping(value = "/books")
    public Page<Book> getAll(@RequestParam Pageable pageable) {
        return bookRepository.findAll(pageable);
    }

    @GetMapping("/books/{id}")
    public Optional<Book> getBookById(@PathVariable long id) {
        return bookRepository.findById(id);
    }
}
