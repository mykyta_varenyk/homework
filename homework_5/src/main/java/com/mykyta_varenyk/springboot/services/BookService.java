package com.mykyta_varenyk.springboot.services;

import com.mykyta_varenyk.springboot.model.Book;

import java.util.Optional;

public interface BookService {
    Optional<Book> findById(Long id);

    Iterable<Book> findAll(int page, int size, String sort, String direction);
}
