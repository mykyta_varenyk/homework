package com.mykyta_varenyk.springboot.services.impl;

import com.mykyta_varenyk.springboot.model.Book;
import com.mykyta_varenyk.springboot.repositories.BookRepository;
import com.mykyta_varenyk.springboot.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.Optional;

public class BookServiceImpl implements BookService {
    @Autowired
    private BookRepository bookRepository;

    @Override
    public Optional<Book> findById(Long id) {
        return bookRepository.findById(id);
    }

    @Override
    public Iterable<Book> findAll(int page, int size, String sortBy, String direction) {
        Sort sort = Sort.by(sortBy);

        if ("desc".equals(direction)) {
            sort.descending();
        }

        return bookRepository.findAll(PageRequest.of(page - 1, 4, sort));
    }
}
