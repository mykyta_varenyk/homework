package com.mykyta_varenyk.aop_namespace;

import com.mykyta_varenyk.aop_namespace.services.BookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AOPNamespaceApplication {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");

        BookService bookService = context.getBean(BookService.class);

        bookService.saveBook("First book", args[0]);

        bookService.saveBook("Second book", args[0]);

        bookService.saveBook("Third book", args[0]);

        bookService.showAllBooks();
    }
}
