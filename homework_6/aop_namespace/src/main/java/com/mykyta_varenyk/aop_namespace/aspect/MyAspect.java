package com.mykyta_varenyk.aop_namespace.aspect;

import org.aspectj.lang.ProceedingJoinPoint;

public class MyAspect {

    public Object aroundAdvice(ProceedingJoinPoint joinPoint) {
        String className = joinPoint.getSignature().getDeclaringTypeName();

        String methodName = joinPoint.getSignature().getName();

        Object[] args = joinPoint.getArgs();

        System.out.println("Execution of " + className + "#" + methodName + " starts");

        try {
            if ("1".equals(args[1])) {
                System.out.println("before advice");

                long before = System.nanoTime();

                Object returnValue = joinPoint.proceed();

                long after = System.nanoTime();

                System.out.println("after advice");

                System.out.println("execution time -> " + (after - before));

                return returnValue;
            }
            throw new IllegalArgumentException("wrong arguments passed");

        } catch (Throwable throwable) {
            throw new RuntimeException("Method " + joinPoint.getSignature().getName() + " threw Exception ->", throwable);
        }
    }
}
