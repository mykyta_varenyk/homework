package com.mykyta_varenyk.aop_namespace.services;

import com.mykyta_varenyk.aop_namespace.annotation.Info;

import java.util.ArrayList;
import java.util.List;

public class BookService {
    private static List<String> books;

    static {
        books = new ArrayList<>();
    }

    @Info
    public void saveBook(String bookName, String consoleArg) {
        books.add(bookName);
        System.out.println(bookName + " saved");
    }

    @Info
    public void showAllBooks() {
        for (String book : books) {
            System.out.println(book);
        }
    }

}
