package com.mykyta_varenyk.aspect_j;

import com.mykyta_varenyk.aspect_j.services.BookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class AspectJApplication {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext("com.mykyta_varenyk.aspect_j");

        BookService bookService = context.getBean(BookService.class);

        bookService.saveBook("First book", "1");

        bookService.saveBook("Second book", "1");

        bookService.saveBook("Third book", "1");

        bookService.showAllBooks();
    }
}
