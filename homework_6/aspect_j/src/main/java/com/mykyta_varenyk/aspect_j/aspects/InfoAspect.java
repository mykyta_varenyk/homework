package com.mykyta_varenyk.aspect_j.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Random;

@Aspect
@Component
public class InfoAspect {
    @Pointcut("@annotation(com.mykyta_varenyk.aspect_j.annotation.Info)")
    public void infoPointcut() {

    }

    @Around("infoPointcut()")
    public Object aroundAdvice(ProceedingJoinPoint joinPoint) {
        String className = joinPoint.getSignature().getDeclaringTypeName();

        String methodName = joinPoint.getSignature().getName();

        System.out.println("Execution of " + className + "#" + methodName + " starts");

        try {
            long before = System.nanoTime();

            Object returnValue = joinPoint.proceed();

            long after = System.nanoTime();

            System.out.println("execution time -> " + (after - before));

            Random random = new Random();

            int randomNumber = random.nextInt(10);

            if (randomNumber < 3) {
                throw new Exception();
            }

            return returnValue;
        } catch (Throwable throwable) {
            throw new RuntimeException("Method " + joinPoint.getSignature().getName() + " threw Exception ->", throwable);
        }
    }
}
