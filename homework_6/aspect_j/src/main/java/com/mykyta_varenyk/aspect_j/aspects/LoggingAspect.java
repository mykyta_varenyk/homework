package com.mykyta_varenyk.aspect_j.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {
    @Pointcut("execution(* *..save*(..)))")
    public void loggingPointcut() {

    }

    @Before("loggingPointcut()")
    public void beforeAdvice(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();

        if ("1".equals(args[1])) {
            System.out.println("before advice");
        } else {
            try {
                throw new Exception("wrong argument passed");
            } catch (Exception exception) {
                throw new IllegalArgumentException("Method " + joinPoint.getSignature().getName() + " threw Exception ->", exception);
            }
        }
    }
}
