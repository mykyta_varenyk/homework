package com.mykyta_varenyk.aspect_j.services;

import com.mykyta_varenyk.aspect_j.annotation.Info;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BookService {
    private static List<String> books;

    static {
        books = new ArrayList<>();
    }

    @Info
    public void saveBook(String bookName, String consoleArg) {
        books.add(bookName);

        System.out.println(bookName + " saved");
    }

    @Info
    public void showAllBooks() {
        for (String book : books) {
            System.out.println(book);
        }
    }

}
