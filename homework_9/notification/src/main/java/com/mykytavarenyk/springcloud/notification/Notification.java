package com.mykytavarenyk.springcloud.notification;

public class Notification {
    String user;
    Notifier notifyBy = Notifier.EMAIL;

    public Notification() {
    }

    public Notification(String user) {
        this.user = user;
    }

    enum Notifier {
        EMAIL
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Notifier getNotifyBy() {
        return notifyBy;
    }

    public void setNotifyBy(Notifier notifyBy) {
        this.notifyBy = notifyBy;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "user='" + user + '\'' +
                ", notifyBy=" + notifyBy +
                '}';
    }
}

