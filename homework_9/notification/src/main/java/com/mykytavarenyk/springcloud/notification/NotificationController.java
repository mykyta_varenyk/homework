package com.mykytavarenyk.springcloud.notification;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RequestMapping
@RestController
public class NotificationController {
    private final Set<Notification> notifications = new HashSet<>();

    @PostMapping
    public void notify(@RequestBody String user) {
        notifications.add(new Notification(user));
    }

    @GetMapping
    public ResponseEntity<List<Notification>> getNotifications() {
        if (notifications.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ArrayList<>(notifications), HttpStatus.OK);
    }
}
