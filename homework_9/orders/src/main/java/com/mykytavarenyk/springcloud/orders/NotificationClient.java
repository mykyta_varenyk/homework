package com.mykytavarenyk.springcloud.orders;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("notifications")
public interface NotificationClient {
    @PostMapping
    void createNotification(@RequestBody String userName);
}
