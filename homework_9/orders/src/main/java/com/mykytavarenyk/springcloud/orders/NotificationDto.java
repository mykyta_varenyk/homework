package com.mykytavarenyk.springcloud.orders;

public class NotificationDto {
    String user;

    Notifier notifyBy = Notifier.EMAIL;

    public NotificationDto(String user) {
        this.user = user;
    }

    enum Notifier {
        EMAIL
    }

    @Override
    public String toString() {
        return "NotificationDto{" +
                "user='" + user + '\'' +
                ", notifyBy=" + notifyBy +
                '}';
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Notifier getNotifyBy() {
        return notifyBy;
    }

    public void setNotifyBy(Notifier notifyBy) {
        this.notifyBy = notifyBy;
    }
}
