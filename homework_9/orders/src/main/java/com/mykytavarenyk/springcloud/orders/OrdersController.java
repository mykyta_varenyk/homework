package com.mykytavarenyk.springcloud.orders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RequestMapping
@RestController
public class OrdersController {
    private List<Order> orderList = new ArrayList<>();

    private final DiscoveryClient discoveryClient;

    private final UserClient userClient;

    private final ProductClient productClient;

    private final NotificationClient notificationClient;

    @Autowired
    public OrdersController(DiscoveryClient discoveryClient, UserClient userClient, ProductClient productClient, NotificationClient notificationClient) {
        this.userClient = userClient;
        this.discoveryClient = discoveryClient;
        this.productClient = productClient;
        this.notificationClient = notificationClient;
    }

    @GetMapping
    public String health() {
        return "OK";
    }

    @PostMapping
    public ResponseEntity<Order> createNewOrder(@RequestBody Order order) {
        productClient.getProduct(order.getProduct());

        userClient.getUser(order.getUserName());

        notificationClient.createNotification(order.getUserName());

        orderList.add(order);

        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    @GetMapping("/users/{userName}")
    public List<String> getProductsForUser(@PathVariable String userName) {
        return orderList.stream()
                .filter(order -> userName.equals(order.getUserName()))
                .map(Order::getProduct)
                .collect(toList());
    }
}
