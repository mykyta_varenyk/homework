package com.mykytavarenyk.springcloud.orders;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "products")
public interface ProductClient {
    @GetMapping("/{productName}")
    ProductDto getProduct(@PathVariable String productName);
}
