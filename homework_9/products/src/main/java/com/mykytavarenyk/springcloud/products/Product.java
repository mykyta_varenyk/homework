package com.mykytavarenyk.springcloud.products;

public class Product {
    public Product() {
    }

    public Product(String name, Long quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    private String name;
    private Long quantity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
