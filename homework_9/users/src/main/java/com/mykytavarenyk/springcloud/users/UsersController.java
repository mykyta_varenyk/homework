package com.mykytavarenyk.springcloud.users;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RequestMapping
@RestController
public class UsersController {
    private Map<String, User> registeredUsers = new HashMap<>();

    @Qualifier("com.mykytavarenyk.springcloud.users.OrderClient")
    @Autowired
    OrderClient orderClient;

    @PostMapping
    public User createUser() {
        User user = new User(RandomStringUtils.randomAlphabetic(13));
        registeredUsers.put(user.getName(), user);

        return user;
    }

    @GetMapping("/{userName}")
    public ResponseEntity<User> getUser(@PathVariable String userName) {
        User user = registeredUsers.get(userName);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/{userName}/products")
    public List<String> getProductsByUser(@PathVariable String userName) {
        return orderClient.getProducts(userName);
    }

    private ArrayList<String> getDefaultProducts(String value) {
        return new ArrayList<String>(Arrays.asList("One", "Two", "Three"));
    }
}
